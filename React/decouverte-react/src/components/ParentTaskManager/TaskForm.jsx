import { useState } from "react";

function TaskForm(props) {
  // état interne qui stocke le nom de la tâche qu'on va ajouter
  const [taskName, setTaskName] = useState("");

  // on met à jour l'état du state qui stocke le nom de la tâche grâce à la fonction setTaskName avec la valeur de l'input qu'on reçoit grâce à event.target.value
  function handleChange(event) {
    setTaskName(event.target.value);
  }

  // créer une fonction qui est appelée au submit
  function handleSubmit(event) {
    event.preventDefault();
    let newTask = { id: Date.now(), name: taskName, completed: false };
    // On crée une copie du tableau tasks en utilisant l'opérateur de propagation (...) pour étendre le tableau existant, puis en ajoutant la nouvelle tâche newTask à la fin. La nouvelle copie du tableau est stockée dans une constante updatedTasks. Ensuite, la fonction onTasksChange est appelée avec la nouvelle copie du tableau updatedTasks pour mettre à jour l'état des tâches dans le composant TaskManager. Cela déclenche une nouvelle rendu du composant et la mise à jour de l'affichage des tâches avec la nouvelle tâche ajoutée.
    let updatedTasks = [...props.tasksProp, newTask];
    props.onTasksChange(updatedTasks);
    setTaskName("");
  }

  // créer un nouvel objet tâche qui va reprendre la structure id / name / completed
  return (
    <form onSubmit={handleSubmit}>
      {/* Quand l'utilisateur rentre quelque chose dans le input, alors l'action onChange est déclenchée, et du coup la fonction handleChange est appelée. */}
      <input type="text" value={taskName} onChange={handleChange} />
      <button type="submit">Ajouter une tâche</button>
    </form>
  );
}

export default TaskForm;
