

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
   
    <title>niveau_3</title>

    <!-- css --> 

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
    <link rel="stylesheet" href="assets/css/style.css">
 <!-- css fin--> 



</head>
<body>
  <div class="container text-center">
    <div class="row justify-content">
      <div class="col col-lg-2">
        <p class="h5">niveau 3<p>
      </div>
    </div>
  </div>

<br>
<br>
<p class="h5">progression<p>

<div class="progress" role="progressbar" aria-label="Basic example" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
  <div class="progress-bar w-0">
  </div>  
</div>

<br>
<br>
<!-- <div id="n3q1" style="display:none"> -->
<p class="h5">niveau_3_Question_1 <p>
<p class="h1 text-center">Pour mieux te connaître</p>
  <form action="traitement_niveau_3.php" method="POST">

          <select class="form-select" name ="niveau_3_question_1" id="niv3_q1" size="4" aria-label="size 3 select example">
              <option selected>Open this select menu</option>
              <option value="1"> Je sais skier.</option>
              <option value="2"> Je sais surfer.</option>
              <option value="3"> Je sais skier et surfer.</option>
              <option value="-1"> Je ne sais ni skier ni surfer.</option>
          </select>
<!-- <div> -->
<br>
<br>
<br>
<!-- <div id="n3q2" style="display:none"> -->
<p class="h5">niveau_3_Question_2 <p>
<p class="h1 text-center">As-tu ton matériel?</p>
<div class="text-center">

<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_3_question_2" id="niv3_q2" value="1">
  <label class="form-check-label" for="inlineRadio1">Oui</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_3_question_2" id="niv3_q2" value="-1">
  <label class="form-check-label" for="inlineRadio2">Non</label>
</div>
<!-- <div> -->
  
<br>
<br>
<br>
<!-- <div id="n3q3" style="display:none"> -->
<p class="h5">niveau_3_Question_3 <p>
<p class="h1 text-center">En hiver tu fais ?</p>
        <select class="form-select" name ="niveau_3_question_3" id="niv3_q3" size="3" aria-label="size 3 select example">
            <option selected>Open this select menu</option>
            <option value="4"> 1 a 2 sorties par saison.</option>
            <option value="5"> 3 à 5 sorties par saison.</option>
            <option value="6"> Je glisse tous les week-end.</option>
        </select>
<!-- <div> -->
<br>
<br>
<br>
<p class="h5">niveau_3_Question_4 <p>
<p class="h1 text-center">La plus part du temps tu descends les pistes </p>
        <select class="form-select" name ="niveau_3_question_4" id ="niv3_q4" size="3" aria-label="size 3 select example">
            <option selected>Open this select menu</option>
            <option value="7">Sur les fesses</option>
            <option value="8">En chasse-neige</option>
            <option value="9">Tout schuss</option>
        </select>
<br>
<br>
<br>
<!-- <div id="n3q5" style="display:none"> -->
<p class="h5">niveau_3_Question_5 <p>
<p class="h1 text-center">Veux-tu apprendre?</p>
<div class="text-center">



<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_3_question_5" id="niv3_q5" value="1">
  <label class="form-check-label" for="inlineRadio1">Oui</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio" name="niveau_3_question_5" id="niv3_q5" value="-1">
  <label class="form-check-label" for="inlineRadio2">Non</label>
</div>
<!-- <div> -->



<br>
<br>
<br>
<button class="btn btn-secondary" type="submit">Traitement php</button>
<br>
<br>
 
<div class="text-center">
<a class="btn btn-primary btn-lg" href="resultat.php" role="button"> quel skieur tu es !</a>
</div>

  <br>
  <br>
  </form>
<div class="text-center">

  </div>

 


  <!-- JavaScript --> 
  <script src="niveau_3.js"></script>
<!-- JavaScript--> 
</body>
</html>