msgid ""
msgstr ""
"Project-Id-Version: Automatic-File-Renamer\n"
"POT-Creation-Date: 2022-02-24 10:56+0100\n"
"PO-Revision-Date: 2022-02-24 10:58+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.0.1\n"
"X-Poedit-Basepath: ../Admin\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: AFR-admin-Panel.php\n"

#: AFR-admin-Panel.php:109
msgid "Automatic File Renamer"
msgstr "Automatic File Renamer"

#: AFR-admin-Panel.php:111
msgid ""
"Lors du téléversement d'un média, <b>une page liée est automatiquement "
"créée</b>.<br>\n"
"  <p>Si votre fichier a pour nom \"<b>cheval.jpg</b>\", la page s'appelera "
"\"<b>https://votresite.fr/cheval</b>\".\n"
"    <br>"
msgstr ""
"When a media is uploaded, <b>an attachment page is automatically created</b>."
"<br>\n"
"  <p> If your file's name is \"<b>horse.jpg</b>\", the attachment page will "
"be called \"<b>https://your-site.com/horse</b>\".\n"
"    <br>"

#: AFR-admin-Panel.php:114
msgid "Cela peut être très embêtant, pour pleins de raisons "
msgstr "That can be really annoying, for many reasons "

#: AFR-admin-Panel.php:114
msgid "évoquées ici"
msgstr "mentioned here"

#: AFR-admin-Panel.php:115
msgid ""
"D'autant plus qu'une fois que la page est créée, il est très dangereux de la "
"renommer !"
msgstr ""
"Especially since once the page is created, it's very dangerous to rename it !"

#: AFR-admin-Panel.php:116
msgid ""
"Pour éviter cela, il est possible grâce à ce plugin de renommer "
"automatiquement vos fichiers et leur pages liées lors de leur téléversement."
msgstr ""
"To avoid this, it's possible thanks to this plugin to rename automatically "
"your files and their attachment pages while the upload."

#: AFR-admin-Panel.php:118
msgid "Choisissez des termes évocateurs"
msgstr "Choose evocative terms"

#: AFR-admin-Panel.php:119
msgid ""
"Les moteurs de recherche (et vous aussi) aiment quand les mots décrivent le "
"contenu !"
msgstr "Browsers (and you too) love when words describe file content !"

#: AFR-admin-Panel.php:128 AFR-admin-Panel.php:137
msgid "nom-du-fichier"
msgstr "file-name"

#: AFR-admin-Panel.php:130
msgid ".extension"
msgstr ".extension"

#: AFR-admin-Panel.php:133 AFR-admin-Panel.php:202
msgid "Enregistré !"
msgstr "Saved !"

#: AFR-admin-Panel.php:136
msgid "Actuellement, vos fichiers seront renommés ainsi :"
msgstr "Currently,your files will be renamed like this :"

#: AFR-admin-Panel.php:137
msgid ""
"Une fois enregistré, le nom du fichier n'est plus modifiable. Choisissez le "
"nom AVANT de téléverser le fichier."
msgstr ""
"Once saved, the file name cannot be changed. Choose the name BEFORE "
"uploading the file."

#: AFR-admin-Panel.php:137
msgid ""
"Ce nombre varie. il permet de différencier deux enregistrements avec le même "
"nom de fichier."
msgstr ""
"This number changes. it allows to differentiate between two records with the "
"same file name."

#: AFR-admin-Panel.php:137
msgid "extension"
msgstr "extension"

#: AFR-admin-Panel.php:139
msgid ""
"Ce réglage s'appliquera à tous les fichiers qui seront téléversés.<br>Ce "
"n'est pas rétroactif."
msgstr ""
"This setting will apply to all files that will be uploaded.<br>It's not "
"retroactive."

#: AFR-admin-Panel.php:143
msgid "La redirection permet de cacher la page liée."
msgstr "The redirect hides the linked page."

#: AFR-admin-Panel.php:151
msgid "Renvoie sur le fichier média (défaut)."
msgstr "Redirect to the file url (default)."

#: AFR-admin-Panel.php:155
msgid "Renvoie sur la page attachée, si elle existe (sinon erreur 404)"
msgstr "Redirect to the attachment page, if it exists (or else, 404 error)."

#: AFR-admin-Panel.php:159
msgid "Renvoie une erreur 404."
msgstr "Redirect to your 404 page."

#: AFR-admin-Panel.php:173
msgid "Réglages réservés aux administrateurs"
msgstr "Settings only for Administrators"

#: AFR-admin-Panel.php:174
msgid "Choisissez quel(s) role(s) peuvent modifier les réglages ci-dessus :"
msgstr "Choose wich role(s) can change the above settings :"

#: AFR-admin-Panel.php:211
msgid "Documention"
msgstr "Documention"

#: AFR-admin-Panel.php:239
msgid "Renommage activé"
msgstr "Renaming enabled"

#: AFR-admin-Panel.php:243
msgid "Activer le renommage"
msgstr "Enable renaming"

#: AFR-admin-Panel.php:266
msgid "Redirection activée"
msgstr "Redirects enabled"

#: AFR-admin-Panel.php:271
msgid "Activer les redirections"
msgstr "Enable redirects"

#~ msgid ""
#~ "Lors du téléversement d'un média, <b>une page liée est automatiquement "
#~ "créée</b>.<br>\n"
#~ "  <p>Si votre fichier a pour nom \"cheval.jpg\", la page s'appelera "
#~ "\"https://votresite.fr/cheval\".\n"
#~ "    <br>\n"
#~ "  Cela peut être très embêtant si vous vouliez justement écrire une page "
#~ "ou un article, avec cette url... <br>D'autant plus qu'une fois que la "
#~ "page est créée, il est très dangereux de la renommer !</p>\n"
#~ "  <p>Pour éviter cela, il est possible grâce à ce plugin de renommer "
#~ "automatiquement votre fichier lors de son téléversement. </p>"
#~ msgstr ""
#~ "When you upload a media, <b>a attachment page is automatically created</"
#~ "b>.<br>\n"
#~ "  <p>If your file is called \"horse.jpg\", the attachment page will be "
#~ "called \"https://yoursite.com/horse\".\n"
#~ "    <br>\n"
#~ "  That can ben very annoying if you just wanted to write a page or an "
#~ "article with \"horse\" as slug...<br>Especially since once the page is "
#~ "created, it is dangerous to rename it !</p>\n"
#~ "  <p>To avoid this, it is possible thanks to this plugin to rename your "
#~ "files automatically while uploading. </p>"
