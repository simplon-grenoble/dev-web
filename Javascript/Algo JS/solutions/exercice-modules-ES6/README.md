# Découverte des modules ES6

Un projet pour apprendre à utiliser [les modules ES6](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Modules).

## Installation

1. Cloner le projet.
2. Ouvrir GIT BASH dans le dossier du projet.
3. Exécuter "npm install" pour installer les dépendances.
4. Exécuter "npm run dev" pour créer le build de développement.
6. Lancer le fichier index.html du répertoire "dist" pour lancer la web app.

## Plus d'infos sur Webpack

- [Tutoriel Webpack 5 pour les débutants](https://www.gekkode.com/developpement/tutoriel-webpack-5-pour-les-debutants/).
- [Comprendre webpack dans un projet front-end JavaScript](https://www.axopen.com/blog/2021/10/webpack-projet-front-end-tuto/).

## Comment ça marche ?

Npm (pour Node Package Manager) va permettre de gérer les projets javascript (et front-web en général).
Ca va consister en un fichier package.json qui fera office de carte d'identité de notre projet. Le fichier va contenir toutes les informations utiles à savoir concernant notre projet notamment :
- Le nom du projet
- La version actuelle du projet (en semantic versionning)
- Les dépendances du projet : c'est à dire tout ce dont le projet à besoin comme ressources externes pour pouvoir fonctionner. Par exemple, si mon projet utilise bootstrap, le package.json contiendra une ligne indiquant la version de bootstrap utilisé.

Pour créer un projet npm, les étapes sont les suivantes : 
1. Créer le dossier qui contiendra le projet et placer un terminal de commande au niveau de ce dossier
2. Faire un `npm init -y` pour initialiser un projet npm avec les valeurs par défaut
3. Ajouter un fichier .gitignore contenant `node_modules` pour indiquer à git qu'il doit ignorer le dossier node_modules et son contenu

Si on veut utiliser webpack on ajoutera les étapes :
4. Faire un `npm install webpack webpack-cli --save-dev` pour 
installer webpack
5. Créer un fichier webpack.config.js pour indiquer votre (ou vos) points d'entrée js et le nom du bundle généré
6. Ajouter un script dans le package.json qui permettra de lancer 
webpack `"dev":"webpack --watch"` par exemple
7. Lancer le script dans le terminal avec `npm run dev`
8. Faire que votre fichier html pointe sur le bundle js généré

(le point 7 sera à répété à chaque fois qu'on veut utiliser le projet)

Pour utiliser un projet npm : 
- Pour rajouter une nouvelle dépendance, on utilise la commande `npm install jquery --save` (jquery est à remplacer par le nom de la dépendance à installer)
- Pour cloner un projet utilisant npm, on commence par faire un git clone, puis à l'intérieur du projet on lancera un `npm install` afin que npm aille récupérer toutes les dépendances du projet listées dans le package.json

Exemple de structure de fichiers d'un projet JS :

* node_modules
* src
    * js
        * index.js
    * css
    * img
* dist
    * index.html
* package.json
* .gitignore

node_modules : contient toutes les librairies et dépendances récupérées avec npm, on y touche pas.

src : c'est le dossier qui contiendra nos fichiers à nous (nos sources), ces fichiers contiennent notre code et peuvent utiliser des choses qui viennent du node_modules.