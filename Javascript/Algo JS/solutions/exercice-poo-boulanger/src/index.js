import { Baker } from "./Baker";
import { Bread } from "./Bread";
import { FrenchPerson } from "./FrenchPerson";

let pain = new Bread(250, "baguette");
console.log(pain);

let person = new FrenchPerson(1000, "Pierre");
console.log("La personne a faim", person.hunger);
person.eat(pain);
person.eat(pain);
person.eat(pain);
person.eat(pain);
person.eat(pain);
person.eat(pain);
console.log("Elle a mangé", person.hunger);

let boulanger = new Baker("Paul", 500);
console.log("Quantité de farine", boulanger.quantiteDeFarine);
boulanger.cuireDesPains(200);
console.log("Quantité de farine", boulanger.quantiteDeFarine);