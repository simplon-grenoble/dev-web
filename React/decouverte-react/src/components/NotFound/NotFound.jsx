function Error() {
  return (
    <p>Oups... Il semblerait que la page que vous cherchez n’existe pas</p>
  );
}

export default Error;
