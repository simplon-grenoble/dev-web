import { Loader } from "../../utils/style/Atoms";
import styled from "styled-components";
import { useEffect, useReducer } from "react";

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

// Etat initial
const initialState = {
  recettesList: [],
  isDataLoading: false,
  error: false,
  searchValue: "",
};

/************************************************************

POURQUOI UTILISER UN REDUCER : Il est préférable de minimiser le nombre de states dans React pour éviter de rendre le code difficile à gérer. Pour cela, l'utilisation d'un reducer est une pratique courante pour gérer l'état global de l'application de manière centralisée, en évitant ainsi la multiplication des states dans les différents composants de l'application.

Le reducer est une fonction qui prend en entrée un état actuel et une action, et renvoie un nouvel état en fonction de l'action effectuée. On peut ainsi facilement modifier l'état global de l'application de manière prévisible et contrôlée. 

Cependant, l'utilisation de plusieurs states peut être justifiée dans certains cas pour stocker des données distinctes et non liées les unes aux autres. Les bugs qu'il est possible de rencontrer en cas de gestion non centralisées du state sont :

- Des incohérences dans l'état : si les différents states ne sont pas synchronisés de manière appropriée, vous pourriez rencontrer des incohérences dans l'état de l'application. Par exemple, si vous avez deux states représentant une même donnée, et qu'une mise à jour est effectuée dans l'un mais pas dans l'autre, cela peut conduire à des erreurs ou à des résultats inattendus.

- Des problèmes de performance : si vous avez plusieurs states qui sont mis à jour fréquemment, cela peut affecter les performances de votre application. En effet, à chaque mise à jour de l'un des states, React doit mettre à jour l'interface utilisateur, ce qui peut conduire à des ralentissements ou des blocages.

- Des erreurs de manipulation de l'état : si vous manipulez les différents states de manière incorrecte, cela peut conduire à des erreurs. Par exemple, si vous modifiez directement l'un des states plutôt que d'utiliser setState(), cela peut conduire à des résultats inattendus.

************************************************************/

// Reducer qui prend en entrée l'état initial de l'application (state) et une action qui sera dispatchée pour modifier cet état.
function reducer(state, action) {
  switch (action.type) {
    // Si l'action correspond à l'envoi de la requête, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie les valeurs des clés isDataLoading (pour la passer à true car on est en train de charger) et error (pour la passer à false car la requête est fonctionnelle).
    case "FETCH_RECETTES_REQUEST":
      return {
        ...state,
        isDataLoading: true,
        error: false,
      };

    // Si l'action correspond au succès de la requête, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie les valeurs des clés recettesList (où on passe en props la valeur de la data recettesList reçue) et isDataLoading (pour la passer à false car on est plus en train de charger)
    case "FETCH_RECETTES_SUCCESS":
      return {
        ...state,
        recettesList: action.data.recettesList,
        isDataLoading: false,
      };

    // Si l'action correspond à l'échec de la requête, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie les valeurs des clés isDataLoading (pour la passer à false car on est plus en train de charger) et error (pour la passer à true car la requête a échoué).
    case "FETCH_RECETTES_FAILURE":
      return {
        ...state,
        isDataLoading: false,
        error: true,
      };

    // Si l'action correspond à un changement du contenu du champ de recherche, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie la valeur de la clé searchValue  (où on passe en props la valeur de la data searchTerm reçue)
    case "SET_SEARCH_VALUE":
      return {
        ...state,
        searchValue: action.data.searchValue,
      };

    default:
      throw new Error();
  }
}

function Recettes() {
  // const [searchValue, setSearchValue] = useState("");
  // const [recettesList, setRecettesList] = useState([]);
  // const [isDataLoading, setIsDataLoading] = useState(false);

  const [state, dispatch] = useReducer(reducer, initialState);

  const handleChange = (event) => {
    // setSearchValue(event.target.value);
    dispatch({
      type: "SET_SEARCH_VALUE",
      data: { searchValue: event.target.value },
    });
  };

  const fetchData = async () => {
    // const response = await fetch(
    //   `https://www.themealdb.com/api/json/v1/1/search.php?s=${searchValue}`
    // );
    // if (!response.ok) {
    //   throw new Error("Data coude note bi fetchet!");
    // } else {
    //   return response.json();
    // }

    // https://javascript.info/async-await
    // https://www.positronx.io/how-to-handle-http-request-with-async-await-in-react/
    try {
      dispatch({ type: "FETCH_RECETTES_REQUEST" });
      const response = await fetch(
        `https://www.themealdb.com/api/json/v1/1/search.php?s=${state.searchValue}`
      );
      return response.json();
    } catch (err) {
      throw new Error(err);
    }
  };

  useEffect(() => {
    fetchData()
      .then((response) => {
        const recettesList = response["meals"];

        dispatch({
          type: "FETCH_RECETTES_SUCCESS",
          data: { recettesList },
          banane: "jaune",
        });

        // Dans le cas où aucune recette ne correspond
        if (!recettesList) {
          dispatch({
            type: "FETCH_RECETTES_SUCCESS",
            data: { recettesList: [] },
          });
          return;
        }
      })
      .catch((e) => {
        dispatch({ type: "FETCH_RECETTES_FAILURE" });
        console.log("Erreur", e.message);
      });
  }, [state.searchValue]);

  return (
    <div>
      <h2>Trouvez votre recette</h2>
      <form>
        <input
          type="text"
          value={state.searchValue}
          onChange={handleChange}
          placeholder="Rechercher une recette..."
        />
      </form>
      {state.isDataLoading ? (
        <LoaderWrapper>
          <Loader />
        </LoaderWrapper>
      ) : (
        <ul>
          {state.recettesList.map((recette, index) => (
            <li key={`${recette.idMeal}-${index}`}>
              <h3>{recette.strMeal}</h3>
              <img src={recette.strMealThumb} alt={recette.strMeal} />
              <p>{recette.strInstructions}</p>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default Recettes;
