import React from "react";
import Moment from "moment";
// import Avatar from "./Avatar";
import InfosUtilisateur from "./InfosUtilisateur";
import InfosCommentaire from "./InfosCommentaire";

// Librairie Moment pour formater des dates
// https://codesource.io/how-to-format-dates-in-reactjs/
// https://momentjs.com/
function formatDate() {
  return Moment().format("DD-MM-YYYY");
}

const commentaire = {
  date: formatDate(),
  texte: "Ceci est un commentaire",
  utilisateur: {
    firstName: "Audrey",
    lastName: "Hepburn",
    avatarUrl:
      "https://artmiens.files.wordpress.com/2018/03/audrey-hepburn-26.jpg",
  },
};

// function formaterNom(user) {
//   return user.firstName + " " + user.lastName;
// }

function affichageCommentaire(propsCommentaire) {
  return (
    <div>
      <InfosUtilisateur utilisateur={propsCommentaire.utilisateur} />
      <InfosCommentaire commentaire={propsCommentaire} />
    </div>
  );
}

// function Avatar(props) {
//   return (
//     <img
//       class="imageUtilisateur"
//       src={props.utilisateur.avatarUrl}
//       alt={props.utilisateur.firstName}
//     ></img>
//   );
// }

// function InfosUtilisateur(props) {
//   return (
//     <div className="InfosUtilisateur">
//       <Avatar utilisateur={props.utilisateur} />
//       <h1>{formaterNom(props.utilisateur)}</h1>
//     </div>
//   );
// }

// function InfosCommentaire(props) {
//   return (
//     <div className="InfosCommentaire">
//       <p>{props.commentaire.texte}</p>
//       <h4>{props.commentaire.date}</h4>
//     </div>
//   );
// }

class Commentaire extends React.Component {
  render() {
    return affichageCommentaire(commentaire);
  }
}

export default Commentaire;
