<?php
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Hasher mon mot de passe</h1>
  <form>
    <label for="mdp">Mot de passe : </label><input type="text" id="mdp" required>
    <input type="button" value="Allons-y !" onclick="hashermdp()">
  </form>
  <div id="barre"></div>
  <div id="hash"></div>

  <script>
    function hashermdp(){

      var mdp = document.getElementById('mdp').value;
      var barre = document.getElementById('barre');
      var retour = document.getElementById('hash');

      var req = new XMLHttpRequest();
      req.open('POST','traitement.php' , true );
      req.setRequestHeader("Content-Type","application/x-www-form-urlencoded")

      // var data = JSON.stringify({'mdp' : mdp});
      var data = 'mdp=' + mdp;

      req.send(data);

      req.onreadystatechange = function(){
        for (var i = 1; i < 5; i++) {

          if (req.readyState === 1) {
            barre.innerHTML = '<progress value="0" max="100"></progress><span id="SpanProgressBar"> 0%</span>'; }
          if (req.readyState === 2) {
            barre.innerHTML = '<progress value="25" max="100"></progress><span id="SpanProgressBar"> 25%</span>'; }
          if (req.readyState === 3) {
            barre.innerHTML = '<progress value="50" max="100"></progress><span id="SpanProgressBar"> 50%</span>'; }
          if (req.readyState === 4) {
            barre.innerHTML = '<progress value="75" max="100"></progress><span id="SpanProgressBar"> 75%</span>'; }
        }
        if ((req.readyState === 4) && (req.status === 200)) {
          barre.innerHTML = '<progress value="100" max="100"></progress><span id="SpanProgressBar"> 100%</span>';
          retour.innerHTML += req.responseText;
        }
      }
    }

  </script>
</body>


</html>
