<?php
require 'init.php';

if (!isset($_SESSION['perso_actuel'])) {
  header('location:index.php?error=connexion');
  exit;
}
$joueur = $_SESSION['perso_actuel'];



if (isset($_POST['victime'])) {

  // Parce que le chiffre envoyé est considéré comme une string, il faut le passer en int pout pouvoir récupérer le perso par son id :
  // var_dump($_POST['victime']);
  $id_victime = (int) $_POST['victime'];
  $repo = new PersonnageRepository();
  $victime = $repo->getPerso($id_victime);

  $joueur->attaque($victime);

  $repo->updatePerso($joueur);
  if ($victime->getVie() == 0 ) {
    $repo->deletePerso($victime->getId());
  } else {
    $repo->updatePerso($victime);
  }

}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Arène de combat</title>
  <link rel="stylesheet" href="_web/css/style.css">
</head>
<body>
  <script src="_web/js/barreDeVie.js"></script>
  <script src="_web/js/ennemi.js"></script>
  <a href="deconnexion.php" class="deconnexion">Quitter le jeu</a>

  <!-- Carte Du joueur -->

  <div id="cartejoueur" class="cartejoueur <?= $joueur->getType()?>">
    <h2><?= $joueur->getNom() ?></h2>
    <h4><?= $joueur->getType() ?></h4>
    <div id="barredeviejoueur"></div>
    <script>
      let BarreDeVieJoueur = new BarreDeVie('BarreDeVieJoueur','barredeviejoueur','cartejoueur',<?= $joueur->getVie() ?>);
    </script>
    <div class="attributs">
      <div class="force">
        <h4>Force</h4>
        <p class="chiffre"><?= $joueur->getForce() ?></p>
      </div>
      <div class="resistance">
        <h4>Resistance</h4>
        <p class="chiffre"><?= $joueur->getResistance() ?></p>
      </div>
    </div>
  </div>

<!-- Cartes des ennemis -->

<div id="ennemis" class="ennemis">
<?php
$db = new Database();
$allEnnemies = $db->getAllPersos();

foreach ($allEnnemies as $ennemi) {
  if($ennemi->Id_perso != $joueur->getId()) {
?>
  <form id="form_<?= $ennemi->Id_perso ?>" action="#" method="POST">
    <input type="hidden" name="victime" value="<?= $ennemi->Id_perso ?>">
    <div id="ennemi_<?= $ennemi->Id_perso ?>" class="carteennemi <?= $ennemi->Type_perso ?>" onclick="document.getElementById('form_<?= $ennemi->Id_perso ?>').submit();
    document.body.style.cursor = 'wait';">
      <h2><?= $ennemi->Nom_perso ?></h2>
      <h4><?= $ennemi->Type_perso ?></h4>
      <div id="barredevie_<?= $ennemi->Id_perso ?>"></div>
      <script>
        let barredevie_<?= $ennemi->Id_perso ?> = new BarreDeVie('barredevie_<?= $ennemi->Id_perso ?>','barredevie_<?= $ennemi->Id_perso ?>','<?= $ennemi->Id_perso ?>',<?= $ennemi->Vie_perso ?>);
      </script>
      <div class="attributs">
        <div class="force">
          <h4>Force</h4>
          <p class="chiffre"><?= $ennemi->Force_perso ?></p>
        </div>
        <div class="resistance">
          <h4>Resistance</h4>
          <p class="chiffre"><?= $ennemi->Resistance_perso ?></p>
        </div>
      </div>
    </div>
  </form>
<?php }
} ?>

</div>




</body>
</html>

