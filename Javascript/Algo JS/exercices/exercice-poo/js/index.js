
/*
 * En javascript, on peut créer des objets à la volet en utilisant
 * des accolades et en spécifiant entre celle ci les différentes
 * propriétés de cet objet.
 * Un objet sera en gros une suite d'associations clef:valeur
 * document et un peu tout ce qui en découle sont des objets.
 * les Array sont des objets etc.
 */

//Pour accéder aux propriétés d'un objet, on utilise le nom de
//la variable qui contient l'objet suivi d'un point et du nom de la propriété ciblée

//Et si la valeur d'une propriété est un objet également, on peut
//enchaîner les points pour accéder aux propriétés de ces sous objets