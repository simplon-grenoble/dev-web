import { useParams } from "react-router";
import { Link } from "react-router-dom";

function Questions() {
  const { questionNumber } = useParams();
  const nextQuestionNumber = parseInt(questionNumber) + 1;
  const previousQuestionNumber =
    parseInt(questionNumber) === 1 ? 1 : parseInt(questionNumber) - 1;

  return (
    <div>
      <h1>Questionnaire</h1>
      <h2>Question n° : {questionNumber}</h2>
      {parseInt(questionNumber) === 1 ? (
        ""
      ) : (
        <Link to={`/questions/${previousQuestionNumber}`}>
          Question précédente
        </Link>
      )}
      <Link to={`/questions/${nextQuestionNumber}`}>Question suivante</Link>
    </div>
  );
}

export default Questions;
