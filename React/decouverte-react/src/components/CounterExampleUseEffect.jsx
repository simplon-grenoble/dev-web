import React, { useEffect } from "react";
import { useState } from "react";

// class Counter extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { counter: 0 };

//     // En utilisant bind(), vous pouvez lier explicitement la méthode à l'objet composant, de sorte que son this sera toujours le composant et non l'objet événement.
//     this.handleClick = this.handleClick.bind(this);
//   }

//   handleClick(change) {
//     this.setState({ counter: this.state.counter + change });
//   }

//   render() {
//     return (
//       <div>
//         <p>Mon compteur : {this.state.counter}</p>
//         {/* https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Functions/Arrow_functions */}
//         {/* <button onClick={() => this.handleClick()}>Incrémenter</button> */}
//         {/* <button onClick={this.handleClick.bind(this)}>Incrémenter</button> */}
//         {/* <button onClick={this.handleClick}>Incrémenter</button> */}
//         <button onClick={() => this.handleClick(1)}>Incrémenter</button>
//         <button onClick={() => this.handleClick(-1)}>Désincrémenter</button>
//       </div>
//     );
//   }
// }

function Counter(props) {
  const [counter, setCounter] = useState(0);

  /* Le cycle de vie d'un composant React se compose de différentes phases, comme la création, la mise à jour et la suppression. Le hook useEffect permet de définir des effets (ou actions) à exécuter à chaque fois que le composant subit une mise à jour. Par exemple, nous pouvons utiliser useEffect pour :

  - Charger des données depuis une API après le premier rendu du composant ;
  - Rafraîchir l'affichage d'un composant lorsque certaines propriétés ou états ont été modifiés ;
  - Manipuler le DOM directement en réaction aux changements dans le composant.

  Le hook useEffect prend deux arguments : une fonction à exécuter et une liste de dépendances. La fonction est appelée à chaque fois que le composant est mis à jour, et la liste de dépendances permet de spécifier à React quels éléments du composant doivent déclencher l'exécution de la fonction.

  En utilisant useEffect de manière appropriée, nous pouvons faire en sorte que notre composant se comporte de manière réactive et efficace en réponse aux changements de l'utilisateur ou de l'état de l'application.*/
  useEffect(() => {
    // Cette fonction sera appelée après chaque mise à jour de l'état du composant
    console.log("Le compteur a été mis à jour !");
  }, [counter]);

  function handleClick(change) {
    setCounter(counter + change);
  }

  return (
    <div>
      <p>Mon compteur : {counter}</p>
      <button onClick={() => handleClick(1)}>Incrémenter</button>
      <button onClick={() => handleClick(-1)}>Désincrémenter</button>
    </div>
  );
}

export default Counter;
