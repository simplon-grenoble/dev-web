<?php
session_start();
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" />
    <title>Resultat du Quiz</title>
    <link rel="stylesheet" href="./assets/css/main.css">
</head>

<body>
    <!-- Je dois trouver la commande pour cacher les warnings, si l'utilisateur ne choisir pas une réponse "OU" -->
    <div id="container_result">
        <h1>Vos resultats</h1>
        <?php

        $reponse11 = $_POST['question-11'];
        $reponse12 = $_POST['question-12'];
        $reponse13 = $_POST['question-13'];
        $reponse14 = $_POST['question-14'];
        $reponse15 = $_POST['question-15'];

        $Points3 = 0;

        if ($reponse11 == "C") {
            $Points3++;
        }
        if ($reponse12 == "D") {
            $Points3++;
        }
        if ($reponse13 == "A") {
            $Points3++;
        }
        if ($reponse14 == "B") {
            $Points3++;
        }
        if ($reponse15 == "D") {
            $Points3++;
        }

        $Points_3 = ($Points3 * 10);
        $Points_1 = $_SESSION["points1"];
        $Points_2 = $_SESSION["points2"];
        $PointsTotal = $Points_1 + $Points_2 + $Points_3;

        echo "<div id='results'><h3>Vous avez $Points_1&nbsp;points sur les questions de&nbsp;1&nbsp;à&nbsp;5.</h3></div>";

        echo "<div id='results'><h3>Vous avez $Points_2&nbsp;points sur les questions de&nbsp;6&nbsp;à&nbsp;10.</h3></div>";

        echo "<div id='results'><h3>Vous avez $Points_3&nbsp;points sur les questions de&nbsp;11&nbsp;à&nbsp;15.</h3></div>";

        echo "<div id='results'><h3>Vous avez $PointsTotal&nbsp;points sur les questions de&nbsp;1&nbsp;à&nbsp;15.</h3></div>";

        ?>
    </div>

    <h2>Vous avez terminé.e le quiz</h2>

    <a class="next_one" href="./index.php">
        <h3>Start Quiz ><span>> Cliquez&nbsp;ICI</span></h3>
    </a>

</body>

</html>