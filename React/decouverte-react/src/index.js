import "./App.css";
import React from "react";
import ReactDOM from "react-dom/client";
// npm i react-router-dom
// import { Navigate, BrowserRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
// npm i react-router
import { Routes, Route as ReactRoute } from "react-router";
// npm i react-query
import { QueryClient, QueryClientProvider } from "react-query";

import "./index.css";
//import App from "./App";
import { ThemeProvider } from "./context/ThemeContext";
import Header from "./components/Header";
import Footer from "./components/Footer";
import NotFound from "./components/NotFound";

import Home from "./routes/Home";
import Recettes from "./routes/Recettes/RecettesReactQuery";
import Questions from "./routes/Questions";
// import PrivateRoute from "./routes/PrivateRoute/PrivateRoute";

// const isAuthenticated = true;
const root = ReactDOM.createRoot(document.getElementById("root"));
const queryClient = new QueryClient();

root.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      {/* <App /> */}
      <Router>
        <ThemeProvider>
          <Header />
          <Routes>
            <ReactRoute path="/" element={<Home />}></ReactRoute>
            <ReactRoute path="/recettes/" element={<Recettes />}></ReactRoute>
            <ReactRoute
              path="/questions/:questionNumber"
              element={<Questions />}
            ></ReactRoute>
            <ReactRoute path="*" element={<NotFound />}></ReactRoute>
            {/* <ReactRoute
            element={
              isAuthenticated ? <PrivateRoute /> : <Navigate to="/" replace />
            }
          ></ReactRoute> */}
          </Routes>
          <Footer />
        </ThemeProvider>
      </Router>
    </QueryClientProvider>
  </React.StrictMode>
);
