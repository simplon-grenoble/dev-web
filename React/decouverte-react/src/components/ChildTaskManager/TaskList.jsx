import { useState } from "react";
import TaskForm from "./TaskForm";

// TaskList : affiche une liste de tâches et permet à l'utilisateur de marquer une tâche comme terminée.
function TaskList(props) {
  /***** SI GESTION DU STATE DANS LES ENFANTS
  C'est possible de gérer l'état localement dans chaque composant enfant plutôt que de le remonter à un composant parent, mais cela peut entraîner une complexité supplémentaire dans le code et rendre le débogage plus difficile. Dans ce cas, vous devrez utiliser la fonction setState pour mettre à jour l'état local dans chaque composant enfant, et passer une fonction de rappel comme prop du composant enfant pour permettre la mise à jour de l'état dans le composant parent. useState permet d'initialiser l'état local avec les tâches passées via les props. *****/
  const [tasks, setTasks] = useState(props.tasks);

  /* Cette fonction, handleTaskCompletion, sera appelée lorsque l'utilisateur coche une case à cocher (checkbox) pour marquer une tâche comme terminée. Elle prend l'ID de la tâche en question comme argument et met à jour l'état des tâches en définissant la propriété "completed" à true pour la tâche correspondante. Elle appelle ensuite la fonction onTasksChange, fournie via les props, pour mettre à jour la liste de tâches dans l'état de la TaskManager parente. 
  
  La principale différence entre la syntaxe const handleTaskCompletion = (id) => {...} et la syntaxe function handleTaskCompletion(id) {...} est la façon dont la fonction est définie et la façon dont le mot-clé this est géré. En utilisant la syntaxe de fonction fléchée, le mot-clé this est hérité de la portée dans laquelle la fonction est définie. En revanche, avec la syntaxe de déclaration de fonction, le mot-clé this est lié à la portée de la fonction elle-même. */
  const handleTaskCompletion = (id) => {
    const updatedTasks = tasks.map((task) => {
      if (task.id === id) {
        return { ...task, completed: true };
      } else {
        return task;
      }
    });
    /***** Nous mettons à jour l'état local en utilisant setTasks plutôt que de passer les tâches mises à jour via une fonction de rappel comme dans l'approche de levée d'état. *****/
    setTasks(updatedTasks);
  };

  // On crée une copie du tableau tasks en utilisant l'opérateur de propagation (...) pour étendre le tableau existant, puis en ajoutant la nouvelle tâche newTask à la fin. La nouvelle copie du tableau est stockée dans une constante updatedTasks. Ensuite, la fonction setTasks est appelée avec la nouvelle copie du tableau updatedTasks pour mettre à jour l'état des tâches dans le composant TaskManager. Cela déclenche une nouvelle rendu du composant et la mise à jour de l'affichage des tâches avec la nouvelle tâche ajoutée.
  const addTask = (newTask) => {
    const updatedTasks = [...tasks, newTask];
    setTasks(updatedTasks);
  };

  /* Dans la partie return de la fonction, nous avons un élément de liste non ordonnée qui contient une liste d'éléments de liste générés à partir de la liste de tâches fournies via les props. Chaque élément de liste contient une case à cocher (checkbox) et le titre de la tâche. Lorsque l'utilisateur coche une case à cocher, la fonction handleTaskCompletion est appelée pour mettre à jour l'état de la TaskManager parente.*/
  return (
    <ul>
      {tasks.map((task) => (
        <li key={task.id}>
          <input
            type="checkbox"
            checked={task.completed}
            onChange={() => handleTaskCompletion(task.id)}
          />
          {task.title}
        </li>
      ))}
      <TaskForm onAddTask={addTask} />
    </ul>
  );
}

export default TaskList;
