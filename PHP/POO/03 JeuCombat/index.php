<?php
require "init.php";

if (isset($_SESSION['perso_actuel'])) {
  header('location:arene.php');
  exit;
}
 // création d'un nouveau personnage :
if (isset($_POST['nom']) && isset($_POST['type']) && isset($_POST['créer'])) {
  $repository = new PersonnageRepository();
  $repository->createPerso($_POST['nom'],$_POST['type']);
}

// Sélection d'un personnage pour jouer :
if (isset($_POST['Id_perso']) && isset($_POST['choisir'])) {
  $repository = new PersonnageRepository();
  $Id_perso = (int) $_POST['Id_perso'];

  $perso_actuel = $repository->getPerso($Id_perso);

  $_SESSION['perso_actuel'] = $perso_actuel;
  header('location:arene.php');
  exit;
}


// $repo = new PersonnageRepository();
// var_dump( $repo->getPerso('Merlin'));
// echo $repo->supprimerPerso(8);

$db = new Database();
$persos = $db->getAllPersos();

?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Jeu de combat</title>
  <link rel="stylesheet" href="_web/css/style.css">
</head>
<body>
  <?php
  if (isset($_GET['error']) && $_GET['error'] == "connexion") { ?>
    <div class="error-message">
      <p>Veuillez choisir ou créer un personnage pour pouvoir jouer.</p>
    </div>
  <?php } ?>
  <form action="#" method="POST">
    <h3>Créer un nouveau personnage</h3>
    Nom de votre perso : <input type="text" name="nom" required>
    Type de perso :
    <select name="type">
      <option value="Mage">Mage</option>
      <option value="Brute">Brute</option>
      <option value="Archer">Archer</option>
      <option value="Sabreur">Sabreur</option>
    </select>

    <input type="submit" name="créer" value="Créer le personnage">
  </form>
  <form action="#" method="POST">
    <h3>Choisir un personnage existant</h3>
    <select name="Id_perso">
      <?php
      foreach ($persos as $perso) { ?>
        <option value="<?= $perso->Id_perso ?>"><?= $perso->Nom_perso ?></option>
      <?php }

      ?>

    </select>
    <input type="submit" name="choisir" value="Choisir ce personnage">
  </form>
</body>
</html>


