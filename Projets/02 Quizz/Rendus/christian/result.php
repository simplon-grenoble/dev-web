<?php session_start() ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="./assets/images/favicon.ico" type="image/svg+xml" />
    <title>Resultat du Quiz</title>
    <link rel="stylesheet" href="./assets/css/main.css">
</head>

<body>
    <!-- Je dois trouver la commande pour cacher les warnings, si l'utilisateur ne choisir pas une réponse -->
    <div id="container_result">
        <h1>Votre resultat</h1>
        <?php

        $reponse1 = $_POST['question-1'];
        $reponse2 = $_POST['question-2'];
        $reponse3 = $_POST['question-3'];
        $reponse4 = $_POST['question-4'];
        $reponse5 = $_POST['question-5'];

        $Points1 = 0;

        if ($reponse1 == "C") {
            $Points1++;
        }
        if ($reponse2 == "D") {
            $Points1++;
        }
        if ($reponse3 == "A") {
            $Points1++;
        }
        if ($reponse4 == "B") {
            $Points1++;
        }
        if ($reponse5 == "D") {
            $Points1++;
        }

        $Points_1 = $Points1 * 10;
        echo "<div id='results'><h3>Vous avez $Points_1&nbsp;points sur les questions de&nbsp;1&nbsp;à&nbsp;5.</h3></div>";
        $_SESSION["points1"] = $Points_1;
        // var_dump($_SESSION);

        ?>
    </div>

    <h2>Pour continuer avec les questions de 6&nbsp;à&nbsp;10</h2>
    <a class="next_one" href="./quiz_2.php">
        <h3><span>Cliquez ICI</span></h3>
    </a>

</body>

</html>