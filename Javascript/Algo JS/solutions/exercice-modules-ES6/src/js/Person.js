export class Person {

    _nom;
    _dateDeNaissance;
    _adresse;

    constructor(nom, dateDeNaissance, adresse){
        this._nom = nom;
        this._dateDeNaissance = dateDeNaissance;
        this._adresse = adresse;
        console.log("PERSONNE", this._nom, this._dateDeNaissance, this._adresse);
    }

    get dateDeNaissance(){
        return this._dateDeNaissance;
    }

}