import { WeatherDescription } from "./WeatherDescription";

export class WeatherDetails {
  _cityName;
  _countryName;
  _temperature;
  _description;

  constructor(cityName, countryName, temperature, description) {
    this._cityName = cityName;
    this._countryName = countryName;
    this._temperature = temperature;
    this._description = new WeatherDescription(
      description["icon"],
      description["description"]
    );

    console.log(
      "New WeatherDetails",
      this._cityName,
      this._countryName,
      this._temperature,
      this._description
    );
  }

  get content() {
    const li = document.createElement("li");
    li.classList.add("city");

    const markup = `
        <h2 class="city-name" data-name="${this._cityName}">
            <span>${this._cityName}</span>
            <sup>${this._countryName}</sup>
        </h2>
        <div class="city-temp">${this._temperature}<sup>°C</sup></div>
        ${this._description.content}`;

    li.innerHTML = markup;
    return li;
  }
}
