import { WeatherDetails } from "./components/WeatherCard/WeatherDetails.js";

/* La recherche se fait par nom de ville (Paris)
ou avec une virgule entre le nom de ville et le code du pays (Paris, FR).
Pour obtenir ta propre clef d'API c'est ici : https://home.openweathermap.org/users/sign_up */
const apiKey = "9f836203ba6b163eede15b2382b542a4";
// ça c'est la mienne, tu dois la changer car sinon vous allez atteindre la limite de requête par heure

// Le début de l'URL pour utiliser Open Weather Map
const urlStart = "https://api.openweathermap.org/data/2.5/weather?q=";
// Les paramètres à appeler en plus pour avoir le système métrique et la langue française
const paramsUrl = "&units=metric&lang=fr";

// const url2 = `https://api.openweathermap.org/data/2.5/weather?q=${inputVal}&appid=${apiKey}&units=metric&lang=fr`;

const form = document.querySelector(".top-banner form");
const input = document.querySelector(".top-banner input");
const list = document.querySelector(".ajax-section .cities");
form.addEventListener("submit", onFormSubmitted);

function onFormSubmitted(event) {
  // ne recharge pas la page (qui est le comportement par défaut)
  event.preventDefault();

  // on récupère la valeur de la ville cherchée
  let inputValue = input.value;

  // Requête AJAX avec le nom de la ville
  const url = `${urlStart}${inputValue}&appid=${apiKey}${paramsUrl}`;
  console.log("URL", url);

  // https://www.digitalocean.com/community/tutorials/how-to-use-the-javascript-fetch-api-to-get-data-fr
  fetch(url)
    // Le paramètre response prend la valeur de l’objet renvoyé de fetch(url). Utilisez la méthode json() pour convertir response en données JSON.
    .then((response) => response.json())
    .then(function (data) {
      console.log(data);

      //console.log(data.name, data.sys.country, data.main.temp, data.weather[0].description);

      // Cette ligne de code utilise la syntaxe de décomposition ("destructuring" en anglais) en JavaScript pour extraire des valeurs d'un objet et les affecter à des variables distinctes. Plus précisément, cette ligne extrait les propriétés main, name, sys et weather de l'objet data, et les affecte respectivement aux variables du même nom.
      const { main, name, sys, weather } = data;
      let weatherCard = new WeatherDetails(
        name,
        sys.country,
        main.temp,
        weather[0]
      );

      list.appendChild(weatherCard.content);
    })
    .catch(function (error) {
      console.error(
        "Il y a eu un problème avec l'opération fetch : " + error.message
      );
    });
}
