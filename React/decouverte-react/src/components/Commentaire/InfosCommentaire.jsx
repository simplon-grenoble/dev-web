import React from "react";

class InfosCommentaire extends React.Component {
  render() {
    /* Lorsque vous utilisez des propriétés dans votre composant, 
    vous pouvez les extraire en utilisant des accolades pour accéder à leur valeur. 
    Cela s'appelle la "déstructuration" d'objet en JavaScript.*/
    const { commentaire } = this.props;

    return (
      <div className="InfosCommentaire">
        <p>{commentaire.texte}</p>
        <h4>{commentaire.date}</h4>
      </div>
    );
  }
}

export default InfosCommentaire;
