
class Ennemi {
  _id;
  _type;
  _nom;
  _vie;
  _force;
  _resistance;

  constructor(id,type,nom,vie,force,resistance){
    this.id = id;
    this.type = type;
    this.nom = nom;
    this.vie = vie;
    this.force = force;
    this.resistance = resistance;

    this.afficherEnnemi();
  }

  get id(){
    return this._id;
  }
  set id(id){
    this._id = id;
  }
  get type(){
    return this._type;
  }
  set type(type){
    this._type = type;
  }
  get nom(){
    return this._nom;
  }
  set nom(nom){
    this._nom = nom;
  }
  get vie(){
    return this._vie;
  }
  set vie(vie){
    this._vie = vie;
  }
  get force(){
    return this._force;
  }
  set force(force){
    this._force = force;
  }
  get resistance(){
    return this._resistance;
  }
  set resistance(resistance){
    this._resistance = resistance;
  }

  afficherEnnemi(){
    let conteneur = document.getElementById('ennemis');
    conteneur.innerHTML += `
    <form id="form_${this.id}" action="#" method="POST">
      <input type="hidden" name="victime" value="${this.id}">
      <div id="ennemi_${this.id}" class="carteennemi ${this.type}" onclick="document.getElementById('form_${this.id}').submit();
      document.body.style.cursor = 'wait';">
        <h2>${this.nom}</h2>
        <h4>${this.type}</h4>
        <div id="barredevie_${this.id}"></div>

        <div class="attributs">
          <div class="force">
            <h4>Force</h4>
            <p class="chiffre">${this.force}</p>
          </div>
          <div class="resistance">
            <h4>Resistance</h4>
            <p class="chiffre">${this.resistance}</p>
          </div>
        </div>
      </div>
    </form>`;


    let barredevie = new BarreDeVie(`barredevie_${this.id}`,`barredevie_${this.id}`,this.id,this.vie);

  }
}
