/**
 * Le Javascript modulaire signifie que chaque fichier js ne 
 * connaîtra QUE les variables/fonctions/classes qu'on lui
 * aura définie à l'intérieur de ce fichier, ou alors qu'on aura
 * explicitement importées d'un autre fichier.
 * Exemple : Je suis dans index.js, si je veux utiliser la classe
 * Address située dans le fichier address.js, il faut que je dise
 * à mon script que j'importe la classe Address du fichier address.js
 * C'est ce qu'on fait la ligne juste en dessous.
 */


/**
 * C'est le même principe pour les librairies, 
 * si je veux utiliser une librairie qu'on a installée avec npm install, 
 * on doit importer celle ci mais pour les libs, 
 * on ne met pas de ./ mais juste le nom de la librairie dans le from.
 */