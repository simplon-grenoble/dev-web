=== Connect Log ===
Contributors: Théophile
Donate Link:
Tags: Log, connexion, connection, admin panel
Requires at least: 5.6
Tested up to: 6.1.1
Stable tag: 0
Requires PHP: 7.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Connect Log permet de garder une trace de la dernière connexion.

== Description ==
Connect Log permet de garder une trace de la dernière connexion, et à l'afficher sur le tableau de bord de l'utilisateur connecté, s'il a accès à l'administration wordpress.

== Changelog ==

= v 0.0 =
01/02/2023
Lancement du projet.
