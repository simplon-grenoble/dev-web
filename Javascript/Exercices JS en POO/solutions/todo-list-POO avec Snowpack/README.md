# Programmation Orientée Objet
Un projet pour apprendre la POO avec JS

## How To use

1. Cloner le projet.
2. Ouvrir GIT BASH dans le dossier du projet.
3. Exécuter "npm i" pour installer les dépendances.
4. Exécuter "npm start" pour lancer le projet.
5. Accéder au projet sur http://localhost:8080.

## Structure d'un projet
En général, les projets de développement auront une structure comme on l'a fait pour celui ci avec à la racine :

- package.json => fichier qui contient toutes les informations du projet (sa version, les scripts de lancement, la liste de ses dépendances/librairies).
- README.md => qui contiendra en markdow une présentation du projet.
- .gitignore => pour indiquer les dossiers/fichiers qu'on souhaite que git ne prenne pas en compte (en général on aura le dossier node_modules, dist, et d'autres).
- node_module => dossier contenant tous les fichiers des dépendances du projet qu'on a installé avec npm.
- dist => dossier contient le résultat du build du projet (c'est généralement le contenu de ce dossier qu'on mettra en ligne au moment du déploiement).
- src => dossier contenant les sources, ce que sont les sources va dépendre un peu selon les projets, mais en JS, on aura les fichiers javascripts.
- public => dossier contenant les fichiers static, comme le html, le favicon, potentiellement le css et les graphismes.

## Exercice TodoList POO [point d'entrée](src/todo-oop.js) / [Task](src/todo/Task.js) / [TodoList](src/todo/TodoList.js)

### Fichier de traduction JSON
1. Créer le contenu du fichier "translation.json" avec 3 valeurs "task","addTask" et "clear" qui correspondent aux affichages à l'écran et leur donner des valeurs (la traduction française).
2. Charger le fichier "/dist/translation.json" grâce à un XMLHttpRequest().
3. Ecouter le onload() afin d'utiliser la request.response pour mettre la bonne traduction sur les éléments HTML à cibler.

### Création des classes
1. Créer une classe Task qui ressemblera à ça (2 propriété, une string et un boolean, et une méthode) 
![task class](diagrammes/todo-task.png)
2. La méthode toggleDone fera en sorte de passer la valeur de la propriété done de true à false ou de false à true.
3. Créer une classe TodoList qui aura une propriété tasks qui contiendra un tableau de Task, ainsi qu'une méthode addTask() et une méthode clearDone().
![TodoList class](diagrammes/todo-list.png)
4. La méthode addTask() permet d'ajouter une nouvelle tâche. Elle va attendre une chaîne de caractère en argument et s'en servir pour faire une nouvelle instance de Task et l'ajouter dans le tableau tasks de la TodoList.
5. La méthode clearDone() permet de supprimer les tasks qui sont terminées. Elle aura pour objectif de parcourir les différentes Tasks de la TodoList et de supprimer celles qui ont la propriété done à true.

### Affichage de la TodoList
1. Dans la class Task, rajouter une méthode toHTML qui va utiliser le createElement pour générer les élément HTML d'une Task. Aide : en l'occurrence, on veut que ça soit par exemple un li avec le label de la Task dedans et un input type checkbox qui sera checked ou non selon si la task est done ou non, et il faudra ensuite return l'élément créé pour pouvoir l'append où on veut.
2. Maintenant qu'on a une Task en HTML, le problème c'est que le HTML n'est pas vraiment lié aux données de la Task, c'est à dire que si on coche ou décoche la checkbox, ça change pas la valeur de la propriété done. Il faut donc faire en sorte que ça la modifie. Aide : dans le toHTML, on peut rajouter un event listener sur la checkbox, et faire que ça déclenche le toggleDone par exemple (ne pas hésiter à tester en mettant un ptit console log de this dans l'event listener).
3. Dans la class TodoList, on va également rajouter une méthode toHTML qui aura pour objectif de générer le HTML de la TodoList même, mais aussi de toutes les Tasks qu'elle contient. Aide : la structure va être la même que pour la Task, avec create element puis return, mais entre les deux, il va falloir boucler sur chaque task pour générer leur HTML avec la méthode faite à l'étape 1.
4. Dans le HTML, on rajoute un ptit formulaire ainsi qu'un bouton Clear, et côté point d'entrée js, on rajoute des event listeners dessus pour qu'ils déclenchent la méthode addTask et la méthode clearDone respectivement, en regénérant le HTML de la TodoList après coup.