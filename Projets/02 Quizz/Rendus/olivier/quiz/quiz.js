// code JS pour avoir l'image pop qui suit le curseur

document.addEventListener("DOMContentLoaded", function() {
    const corn = document.querySelector("#corn");
    var pop = document.querySelector('.pop');
  
    corn.addEventListener("mousemove", function(event) {
        var x = event.clientX;
        var y = event.clientY;
        pop.style.top = y - 100 + "px";
        pop.style.left = x + 100 + "px";
    });
  });