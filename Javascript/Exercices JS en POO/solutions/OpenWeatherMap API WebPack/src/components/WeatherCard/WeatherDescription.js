export class WeatherDescription {
    
  _icon;
  _description;

  constructor(icon, description) {
    this._icon = icon;
    this._description = description;
  }

  get content() {
    const markup = `<figure>
                <img class="city-icon" src="https://openweathermap.org/img/wn/${this._icon}@2x.png" alt="${this._description}">
                <figcaption>${this._description}</figcaption>
            </figure>`;
    return markup;
  }
}
