<?php

class PersonnageRepository {

  private $db;

  // On commence par créer la connexion :

  public function __construct(){
    $this->db = new Database();
    $this->db = $this->db->getBDD();
  }

  ///////////////
  // METHODES  //
  ///////////////

  /**
 * Methode permettant de récupérer un objet Personnage construit.
 * @param  mixed $perso  Soit l'ID du perso (int), soit le nom du perso (string)
 * @return Personnage        Retourne un Personnage instancié.
 */
  public function getPerso($perso)   {
    // En fonction de si on nous donne l'id ou le nom, on cherche la ligne du perso dans la DB.
    if (is_string($perso)) {
      $sql = "SELECT * FROM personnages WHERE Nom_perso = :perso ;";
    }else{
      $sql = "SELECT * FROM personnages WHERE Id_perso = :perso ;";
    }
    $requete = $this->db->prepare($sql);

    $requete->execute([':perso'=>$perso]);

    $infos = $requete->fetch(PDO::FETCH_ASSOC);

    // Une fois qu'on l'a récupéré, on modifie le tableau. En effet, les clés dans $infos ont les noms des colonnes : Id_perso, Nom_perso ... Or, pour construire un objet, la méthode hydrate a besoin des attributs, sans le "_perso". Il faut donc reconstruire le tableau, en gardant juste le début des clés : id, nom, ...

    $perso = [];
    foreach($infos as $key => $value){
      $key = explode('_',$key);
      $key = $key[0];
      $perso = array_merge($perso,[$key => $value]);

    }

    // On construit le personnage :
    $perso = new $perso['Type']($perso);
    return $perso;
  }


  /**
   * Permet de créer un personnage, si le nom proposé est libre.
   * @param  string $nom  Le nom du perso
   * @param  string $type Le type de personnage
   * @return string       Un message de réussite ou d'erreur selon.
   */
  public function createPerso(string $nom, string $type){

    if (in_array($type,Personnage::TYPES)) {

      $sql = 'SELECT Nom_perso FROM `personnages` WHERE Nom_perso = "'.$nom.'"; ';
      $nompresent = $this->db->query($sql);
      $nompresent = $nompresent->fetch(PDO::FETCH_ASSOC);

      if (!$nompresent) {
        $sql = "INSERT INTO personnages (`Nom_perso`, `Type_perso`, `Vie_perso`, `Force_perso`, `Resistance_perso`) VALUES (:Nom_perso,:Type_perso,:Vie_perso,:Force_perso,:Resistance_perso)";

        $requete = $this->db->prepare($sql);

        $requete->execute([ ':Nom_perso'=>$nom,
                            ':Type_perso'=>$type,
                            ':Vie_perso'=>Personnage::VIE_DEFAUT,
                            ':Force_perso'=>$type::FORCE,
                            ':Resistance_perso'=>$type::RESISTANCE,
                          ]);

        return "Le personnage $nom a été ajouté à la Base de Données.";
      } else if($nompresent['Nom_perso'] == $nom) {
        return "<p style='color:red;'>Le nom est déjà pris.</p>";
      }
    }


  }

  // Update
  /**
   * Mise à jour du personnage
   * @param Personnage $perso Objet Personnage à enregistrer.
   */
  public function updatePerso(Personnage $perso){
    // On ne change jamais l'ID ou le Nom du perso.
    $this->setVie($perso->getVie(),$perso->getId());
    $this->setForce($perso->getForce(),$perso->getId());
    $this->setResistance($perso->getResistance(),$perso->getId());

    // Ici, on aurait pu faire une commande sql qui faisait ces trois infos d'un coup, plutôt que d'appeler trois méthodes distinctes. Dans ce cas, les getters et setters auraient un intérêt limité, et on n'aurait pas eu besoin de les créer. J'ai choisi de les utiliser, mais ils ne sont pas toujours nécessaires.
  }
  // Delete

  /**
   * Permet de supprimer un personnage de la BDD
   * @param  int    $Id_perso l'Id du personnage à supprimer
   * @return string           message de validation
   */
  public function deletePerso(int $Id_perso){
    $sql = "DELETE FROM personnages WHERE Id_perso = :Id_perso ;";

    $suppression = $this->db->prepare($sql);
    $suppression->execute([':Id_perso'=>$Id_perso]);

    return "perso supprimé";
  }


  // GETTERS ET SETTERS

  /**
   * Permet de récupérer l'ID d'un perso depuis son nom
   * @param  string $Nom_perso Le nom du perso concerné
   * @return int            L'id du perso
   */
  private function getId(string $Nom_perso){
    $sql = "SELECT Id_perso FROM personnages WHERE Nom_perso = :Nom_perso ;";
    $requete = $this->db->prepare($sql);

    $requete->execute([':Nom_perso'=>$Nom_perso]);

    $id = $requete->fetch(PDO::FETCH_ASSOC);
    return $id['Id_perso'];
  }

  /**
   * Permet de récupérer le nom du perso
   * @param  int    $Id_perso L'id du perso concerné
   * @return string           le nom du perso
   */
  private function getNom(int $Id_perso){
    $sql = "SELECT Nom_perso FROM personnages WHERE Id_perso = :Id_perso ; "; // Récupère le nom du perso

    $requete = $this->db->prepare($sql);

    $exec = $requete->execute([':Id_perso'=>$Id_perso]);

    $nom = $requete->fetch(PDO::FETCH_ASSOC);
    return $nom['Nom_perso'];
  }

  /**
   * Permet de récupérer le type de personnage dans la BDD.
   * @param  mixed $perso Soit l'ID du perso (int), soit le nom du perso (string)
   * @return string        Retourne le type de personnage.
   */
  private function getType($perso){

    if (is_string($perso)) {
      $sql = "SELECT Type_perso FROM personnages WHERE Nom_perso = :perso ;";
    }else{
      $sql = "SELECT Type_perso FROM personnages WHERE Id_perso = :perso ;";
    }
    $requete = $this->db->prepare($sql);

    $requete->execute([':perso'=>$perso]);

    $type = $requete->fetch(PDO::FETCH_ASSOC);
    return $type['Type_perso'];
  }

  /**
   * Permet de modifier le type du personnage sélectionné par son ID
   * @param string $type     Le nouveau type
   * @param int    $Id_perso L'ID du perso
   */
  private function setType(string $type, int $Id_perso){

    if (in_array($type, Personnage::TYPES)) {

      $sql = "UPDATE personnages SET Type_perso = :type_perso WHERE Id_perso = :Id_perso ; ";

      try{
        $requete = $this->db->prepare($sql);

        $requete->bindParam(':type_perso', $type,PDO::PARAM_STR);
        $requete->bindParam(':Id_perso', $Id_perso,PDO::PARAM_INT);

        $requete->execute();

      }catch(PDOException $e){
        echo "erreur de modification du type : ". $e->getMessage();
      }
    } else {
      echo "Le type $type n'est pas valide.";
    }

  }

  /**
   * Permet de récupérer la vie du personnage dans la BDD.
   * @param  mixed $perso Soit l'ID du perso (int), soit le nom du perso (string)
   * @return string        Retourne la vie du personnage.
   */
  private function getVie($perso){

    if (is_string($perso)) {
      $sql = "SELECT Vie_perso FROM personnages WHERE Nom_perso = :perso ;";
    }else{
      $sql = "SELECT Vie_perso FROM personnages WHERE Id_perso = :perso ;";
    }
    $requete = $this->db->prepare($sql);

    $requete->execute([':perso'=>$perso]);

    $vie = $requete->fetch(PDO::FETCH_ASSOC);
    return $vie['Vie_perso'];
  }

/**
 * Permet de modifier la vie du personnage
 * @param float $Vie      la nouvelle vie à enregistrer
 * @param int   $Id_perso l'ID du perso concerné
 */
  private function setVie(float $Vie, int $Id_perso){

    $sql = "UPDATE personnages SET Vie_perso = :Vie_perso WHERE Id_perso = :Id_perso ; ";

    try{
      $requete = $this->db->prepare($sql);

      $requete->bindParam(':Vie_perso', $Vie,PDO::PARAM_STR);
      $requete->bindParam(':Id_perso', $Id_perso,PDO::PARAM_INT);

      $requete->execute();

    }catch(PDOException $e){
      echo "erreur de modification du type : ". $e->getMessage();
    }
  }

  /**
   * Permet de récupérer la force du personnage dans la BDD.
   * @param  mixed $perso Soit l'ID du perso (int), soit le nom du perso (string)
   * @return string        Retourne la force du personnage.
   */
  private function getForce($perso){

    if (is_string($perso)) {
      $sql = "SELECT Force_perso FROM personnages WHERE Nom_perso = :perso ;";
    }else{
      $sql = "SELECT Force_perso FROM personnages WHERE Id_perso = :perso ;";
    }
    $requete = $this->db->prepare($sql);

    $requete->execute([':perso'=>$perso]);

    $force = $requete->fetch(PDO::FETCH_ASSOC);
    return $force['Force_perso'];
  }

/**
 * Permet de mettre à jour la force du perso
 * @param float $Force    la force à enregistrer
 * @param int   $Id_perso l'ID du perso concerné
 */
  private function setForce(float $Force, int $Id_perso){

    $sql = "UPDATE personnages SET Force_perso = :Force_perso WHERE Id_perso = :Id_perso ; ";

    try{
      $requete = $this->db->prepare($sql);

      $requete->bindParam(':Force_perso', $Force,PDO::PARAM_STR);
      $requete->bindParam(':Id_perso', $Id_perso,PDO::PARAM_INT);

      $requete->execute();

    }catch(PDOException $e){
      echo "erreur de modification du type : ". $e->getMessage();
    }
  }

  /**
   * Permet de récupérer la résistance du personnage dans la BDD.
   * @param  mixed $perso Soit l'ID du perso (int), soit le nom du perso (string)
   * @return string        Retourne la résistance du personnage.
   */
  private function getResistance($perso){

    if (is_string($perso)) {
      $sql = "SELECT Resistance_perso FROM personnages WHERE Nom_perso = :perso ;";
    }else{
      $sql = "SELECT Resistance_perso FROM personnages WHERE Id_perso = :perso ;";
    }
    $requete = $this->db->prepare($sql);

    $requete->execute([':perso'=>$perso]);

    $resistance = $requete->fetch(PDO::FETCH_ASSOC);
    return $resistance['Resistance_perso'];
  }

/**
 * Permet de mettre à jour la résistance du perso
 * @param float $Resistance la résistance à enregistrer
 * @param int   $Id_perso   l'ID du perso concerné
 */
  private function setResistance(float $Resistance, int $Id_perso){

    $sql = "UPDATE personnages SET Resistance_perso = :Resistance_perso WHERE Id_perso = :Id_perso ; ";

    try{
      $requete = $this->db->prepare($sql);

      $requete->bindParam(':Resistance_perso', $Resistance,PDO::PARAM_STR);
      $requete->bindParam(':Id_perso', $Id_perso,PDO::PARAM_INT);

      $requete->execute();

    }catch(PDOException $e){
      echo "erreur de modification du type : ". $e->getMessage();
    }
  }

}
