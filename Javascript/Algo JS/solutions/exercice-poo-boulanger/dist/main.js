/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/Baker.js":
/*!**********************!*\
  !*** ./src/Baker.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Baker": () => (/* binding */ Baker)
/* harmony export */ });
/* harmony import */ var _Bread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Bread */ "./src/Bread.js");


class Baker{

    _nom;
    _quantiteDeFarine;

    constructor(nom, quantiteDeFarine){
        this._nom = nom;
        this._quantiteDeFarine = quantiteDeFarine;
    }

    get nom(){
        return this._nom;
    }

    set nom(nom){
        this._nom = nom;
    }

    get quantiteDeFarine(){
        return this._quantiteDeFarine;
    }

    set quantiteDeFarine(quantiteDeFarine){
        this._quantiteDeFarine = quantiteDeFarine;
    }

    // ALT + Z pour retour à la ligne
    cuireDesPains(quantiteDePain){
        // Faire que la méthode bakeBreads crée un tableau vide 
        let fournee = [];
        // puis fasse une boucle qui fera quantiteDePain tours 
        for(let i = 0; i < quantiteDePain; i++){
            // le boulanger fait du pain uniquement s'il lui reste de la farine (quantiteDeFarine supérieur à 0).
            if(this.quantiteDeFarine > 0){
                // et qui à chaque tour de boucle créera une instance de Bread, 
                let pain = new _Bread__WEBPACK_IMPORTED_MODULE_0__.Bread(250, "baguette");
                // déclenchera sa méthode cook et le mettra dans le tableau. 
                pain.cook(300);
                // on ajoute le pain au tableau de la fournée
                fournee.push(pain);
                // chaque fois qu'on fait un pain, on baisse la valeur de la propriété quantiteDeFarine de 1 
                // this.quantiteDeFarine -= 1;
                this.quantiteDeFarine--;
            }            
        }   

        // A la fin de la boucle, la méthode return le tableau de Bread.
        return fournee;
    }

}

/***/ }),

/***/ "./src/Bread.js":
/*!**********************!*\
  !*** ./src/Bread.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Bread": () => (/* binding */ Bread)
/* harmony export */ });
class Bread {

    _weight;
    _type;
    _cooked;

    /*
        @param {number} weight : poids du pain en grammes
        @param {string} type
        @param {boolean} cooked
    */
    constructor(weight, type){
        if(typeof weight !== "number"){
            throw new TypeError("Le poids doit être un nombre");
        }
        if(typeof type !== "string"){
            throw new TypeError("Le type doit être une string");
        }
        this._weight = weight;
        this._type = type;
        this._cooked = false;
    }

    get weight(){
        return this._weight;
    }

    get type(){
        return this._type;
    }

    get cooked(){
        return this._cooked;
    }

    cook(temperature) {
        if(!this._cooked && temperature >= 200 && temperature <= 400){
            this._weight = this._weight * 0.9;
            // this._weight -= this._weight * 0.1;
            // this._weight = this._weight - this._weight * 0.1;
            this._cooked = true;
            return true;
        }
        else {
            return false;
        }
    }

}

/***/ }),

/***/ "./src/FrenchPerson.js":
/*!*****************************!*\
  !*** ./src/FrenchPerson.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FrenchPerson": () => (/* binding */ FrenchPerson)
/* harmony export */ });
class FrenchPerson{

    _hunger;
    _name;

     /*
        @param {number} hunger : faim de la personne
        @param {string} name : nom de la personne
    */
    constructor(hunger, name){
        this._hunger = hunger;
        this._name = name;
    }

    get hunger(){
        return this._hunger;
    }

    get name(){
        return this._name;
    }

    eat(pain){
        if(this._hunger > 0){
            this._hunger -= pain.weight;
        }            
    }

}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Baker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Baker */ "./src/Baker.js");
/* harmony import */ var _Bread__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Bread */ "./src/Bread.js");
/* harmony import */ var _FrenchPerson__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FrenchPerson */ "./src/FrenchPerson.js");




let pain = new _Bread__WEBPACK_IMPORTED_MODULE_1__.Bread(250, "baguette");
console.log(pain);

let person = new _FrenchPerson__WEBPACK_IMPORTED_MODULE_2__.FrenchPerson(1000, "Pierre");
console.log("La personne a faim", person.hunger);
person.eat(pain);
person.eat(pain);
person.eat(pain);
person.eat(pain);
person.eat(pain);
person.eat(pain);
console.log("Elle a mangé", person.hunger);

let boulanger = new _Baker__WEBPACK_IMPORTED_MODULE_0__.Baker("Paul", 500);
console.log("Quantité de farine", boulanger.quantiteDeFarine);
boulanger.cuireDesPains(200);
console.log("Quantité de farine", boulanger.quantiteDeFarine);
})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBZ0M7QUFDaEM7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixvQkFBb0I7QUFDM0M7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLHlDQUFLO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNwRE87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsUUFBUTtBQUN4QixnQkFBZ0IsUUFBUTtBQUN4QixnQkFBZ0IsU0FBUztBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDaERPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixRQUFRO0FBQ3hCLGdCQUFnQixRQUFRO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O1VDNUJBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDdEJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EseUNBQXlDLHdDQUF3QztXQUNqRjtXQUNBO1dBQ0E7Ozs7O1dDUEE7Ozs7O1dDQUE7V0FDQTtXQUNBO1dBQ0EsdURBQXVELGlCQUFpQjtXQUN4RTtXQUNBLGdEQUFnRCxhQUFhO1dBQzdEOzs7Ozs7Ozs7Ozs7OztBQ05nQztBQUNBO0FBQ2M7QUFDOUM7QUFDQSxlQUFlLHlDQUFLO0FBQ3BCO0FBQ0E7QUFDQSxpQkFBaUIsdURBQVk7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLHlDQUFLO0FBQ3pCO0FBQ0E7QUFDQSw4RCIsInNvdXJjZXMiOlsid2VicGFjazovL2pzLXRvb2xzLy4vc3JjL0Jha2VyLmpzIiwid2VicGFjazovL2pzLXRvb2xzLy4vc3JjL0JyZWFkLmpzIiwid2VicGFjazovL2pzLXRvb2xzLy4vc3JjL0ZyZW5jaFBlcnNvbi5qcyIsIndlYnBhY2s6Ly9qcy10b29scy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9qcy10b29scy93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vanMtdG9vbHMvd2VicGFjay9ydW50aW1lL2hhc093blByb3BlcnR5IHNob3J0aGFuZCIsIndlYnBhY2s6Ly9qcy10b29scy93ZWJwYWNrL3J1bnRpbWUvbWFrZSBuYW1lc3BhY2Ugb2JqZWN0Iiwid2VicGFjazovL2pzLXRvb2xzLy4vc3JjL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJyZWFkIH0gZnJvbSBcIi4vQnJlYWRcIjtcclxuXHJcbmV4cG9ydCBjbGFzcyBCYWtlcntcclxuXHJcbiAgICBfbm9tO1xyXG4gICAgX3F1YW50aXRlRGVGYXJpbmU7XHJcblxyXG4gICAgY29uc3RydWN0b3Iobm9tLCBxdWFudGl0ZURlRmFyaW5lKXtcclxuICAgICAgICB0aGlzLl9ub20gPSBub207XHJcbiAgICAgICAgdGhpcy5fcXVhbnRpdGVEZUZhcmluZSA9IHF1YW50aXRlRGVGYXJpbmU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IG5vbSgpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9ub207XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IG5vbShub20pe1xyXG4gICAgICAgIHRoaXMuX25vbSA9IG5vbTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcXVhbnRpdGVEZUZhcmluZSgpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9xdWFudGl0ZURlRmFyaW5lO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBxdWFudGl0ZURlRmFyaW5lKHF1YW50aXRlRGVGYXJpbmUpe1xyXG4gICAgICAgIHRoaXMuX3F1YW50aXRlRGVGYXJpbmUgPSBxdWFudGl0ZURlRmFyaW5lO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFMVCArIFogcG91ciByZXRvdXIgw6AgbGEgbGlnbmVcclxuICAgIGN1aXJlRGVzUGFpbnMocXVhbnRpdGVEZVBhaW4pe1xyXG4gICAgICAgIC8vIEZhaXJlIHF1ZSBsYSBtw6l0aG9kZSBiYWtlQnJlYWRzIGNyw6llIHVuIHRhYmxlYXUgdmlkZSBcclxuICAgICAgICBsZXQgZm91cm5lZSA9IFtdO1xyXG4gICAgICAgIC8vIHB1aXMgZmFzc2UgdW5lIGJvdWNsZSBxdWkgZmVyYSBxdWFudGl0ZURlUGFpbiB0b3VycyBcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgcXVhbnRpdGVEZVBhaW47IGkrKyl7XHJcbiAgICAgICAgICAgIC8vIGxlIGJvdWxhbmdlciBmYWl0IGR1IHBhaW4gdW5pcXVlbWVudCBzJ2lsIGx1aSByZXN0ZSBkZSBsYSBmYXJpbmUgKHF1YW50aXRlRGVGYXJpbmUgc3Vww6lyaWV1ciDDoCAwKS5cclxuICAgICAgICAgICAgaWYodGhpcy5xdWFudGl0ZURlRmFyaW5lID4gMCl7XHJcbiAgICAgICAgICAgICAgICAvLyBldCBxdWkgw6AgY2hhcXVlIHRvdXIgZGUgYm91Y2xlIGNyw6llcmEgdW5lIGluc3RhbmNlIGRlIEJyZWFkLCBcclxuICAgICAgICAgICAgICAgIGxldCBwYWluID0gbmV3IEJyZWFkKDI1MCwgXCJiYWd1ZXR0ZVwiKTtcclxuICAgICAgICAgICAgICAgIC8vIGTDqWNsZW5jaGVyYSBzYSBtw6l0aG9kZSBjb29rIGV0IGxlIG1ldHRyYSBkYW5zIGxlIHRhYmxlYXUuIFxyXG4gICAgICAgICAgICAgICAgcGFpbi5jb29rKDMwMCk7XHJcbiAgICAgICAgICAgICAgICAvLyBvbiBham91dGUgbGUgcGFpbiBhdSB0YWJsZWF1IGRlIGxhIGZvdXJuw6llXHJcbiAgICAgICAgICAgICAgICBmb3VybmVlLnB1c2gocGFpbik7XHJcbiAgICAgICAgICAgICAgICAvLyBjaGFxdWUgZm9pcyBxdSdvbiBmYWl0IHVuIHBhaW4sIG9uIGJhaXNzZSBsYSB2YWxldXIgZGUgbGEgcHJvcHJpw6l0w6kgcXVhbnRpdGVEZUZhcmluZSBkZSAxIFxyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5xdWFudGl0ZURlRmFyaW5lIC09IDE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnF1YW50aXRlRGVGYXJpbmUtLTtcclxuICAgICAgICAgICAgfSAgICAgICAgICAgIFxyXG4gICAgICAgIH0gICBcclxuXHJcbiAgICAgICAgLy8gQSBsYSBmaW4gZGUgbGEgYm91Y2xlLCBsYSBtw6l0aG9kZSByZXR1cm4gbGUgdGFibGVhdSBkZSBCcmVhZC5cclxuICAgICAgICByZXR1cm4gZm91cm5lZTtcclxuICAgIH1cclxuXHJcbn0iLCJleHBvcnQgY2xhc3MgQnJlYWQge1xyXG5cclxuICAgIF93ZWlnaHQ7XHJcbiAgICBfdHlwZTtcclxuICAgIF9jb29rZWQ7XHJcblxyXG4gICAgLypcclxuICAgICAgICBAcGFyYW0ge251bWJlcn0gd2VpZ2h0IDogcG9pZHMgZHUgcGFpbiBlbiBncmFtbWVzXHJcbiAgICAgICAgQHBhcmFtIHtzdHJpbmd9IHR5cGVcclxuICAgICAgICBAcGFyYW0ge2Jvb2xlYW59IGNvb2tlZFxyXG4gICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHdlaWdodCwgdHlwZSl7XHJcbiAgICAgICAgaWYodHlwZW9mIHdlaWdodCAhPT0gXCJudW1iZXJcIil7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJMZSBwb2lkcyBkb2l0IMOqdHJlIHVuIG5vbWJyZVwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYodHlwZW9mIHR5cGUgIT09IFwic3RyaW5nXCIpe1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiTGUgdHlwZSBkb2l0IMOqdHJlIHVuZSBzdHJpbmdcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX3dlaWdodCA9IHdlaWdodDtcclxuICAgICAgICB0aGlzLl90eXBlID0gdHlwZTtcclxuICAgICAgICB0aGlzLl9jb29rZWQgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgd2VpZ2h0KCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3dlaWdodDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdHlwZSgpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLl90eXBlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjb29rZWQoKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29va2VkO1xyXG4gICAgfVxyXG5cclxuICAgIGNvb2sodGVtcGVyYXR1cmUpIHtcclxuICAgICAgICBpZighdGhpcy5fY29va2VkICYmIHRlbXBlcmF0dXJlID49IDIwMCAmJiB0ZW1wZXJhdHVyZSA8PSA0MDApe1xyXG4gICAgICAgICAgICB0aGlzLl93ZWlnaHQgPSB0aGlzLl93ZWlnaHQgKiAwLjk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuX3dlaWdodCAtPSB0aGlzLl93ZWlnaHQgKiAwLjE7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuX3dlaWdodCA9IHRoaXMuX3dlaWdodCAtIHRoaXMuX3dlaWdodCAqIDAuMTtcclxuICAgICAgICAgICAgdGhpcy5fY29va2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufSIsImV4cG9ydCBjbGFzcyBGcmVuY2hQZXJzb257XHJcblxyXG4gICAgX2h1bmdlcjtcclxuICAgIF9uYW1lO1xyXG5cclxuICAgICAvKlxyXG4gICAgICAgIEBwYXJhbSB7bnVtYmVyfSBodW5nZXIgOiBmYWltIGRlIGxhIHBlcnNvbm5lXHJcbiAgICAgICAgQHBhcmFtIHtzdHJpbmd9IG5hbWUgOiBub20gZGUgbGEgcGVyc29ubmVcclxuICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihodW5nZXIsIG5hbWUpe1xyXG4gICAgICAgIHRoaXMuX2h1bmdlciA9IGh1bmdlcjtcclxuICAgICAgICB0aGlzLl9uYW1lID0gbmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaHVuZ2VyKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h1bmdlcjtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbmFtZSgpe1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9uYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGVhdChwYWluKXtcclxuICAgICAgICBpZih0aGlzLl9odW5nZXIgPiAwKXtcclxuICAgICAgICAgICAgdGhpcy5faHVuZ2VyIC09IHBhaW4ud2VpZ2h0O1xyXG4gICAgICAgIH0gICAgICAgICAgICBcclxuICAgIH1cclxuXHJcbn0iLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiLy8gZGVmaW5lIGdldHRlciBmdW5jdGlvbnMgZm9yIGhhcm1vbnkgZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5kID0gKGV4cG9ydHMsIGRlZmluaXRpb24pID0+IHtcblx0Zm9yKHZhciBrZXkgaW4gZGVmaW5pdGlvbikge1xuXHRcdGlmKF9fd2VicGFja19yZXF1aXJlX18ubyhkZWZpbml0aW9uLCBrZXkpICYmICFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywga2V5KSkge1xuXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIGtleSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGRlZmluaXRpb25ba2V5XSB9KTtcblx0XHR9XG5cdH1cbn07IiwiX193ZWJwYWNrX3JlcXVpcmVfXy5vID0gKG9iaiwgcHJvcCkgPT4gKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIHByb3ApKSIsIi8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uciA9IChleHBvcnRzKSA9PiB7XG5cdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuXHR9XG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG59OyIsImltcG9ydCB7IEJha2VyIH0gZnJvbSBcIi4vQmFrZXJcIjtcclxuaW1wb3J0IHsgQnJlYWQgfSBmcm9tIFwiLi9CcmVhZFwiO1xyXG5pbXBvcnQgeyBGcmVuY2hQZXJzb24gfSBmcm9tIFwiLi9GcmVuY2hQZXJzb25cIjtcclxuXHJcbmxldCBwYWluID0gbmV3IEJyZWFkKDI1MCwgXCJiYWd1ZXR0ZVwiKTtcclxuY29uc29sZS5sb2cocGFpbik7XHJcblxyXG5sZXQgcGVyc29uID0gbmV3IEZyZW5jaFBlcnNvbigxMDAwLCBcIlBpZXJyZVwiKTtcclxuY29uc29sZS5sb2coXCJMYSBwZXJzb25uZSBhIGZhaW1cIiwgcGVyc29uLmh1bmdlcik7XHJcbnBlcnNvbi5lYXQocGFpbik7XHJcbnBlcnNvbi5lYXQocGFpbik7XHJcbnBlcnNvbi5lYXQocGFpbik7XHJcbnBlcnNvbi5lYXQocGFpbik7XHJcbnBlcnNvbi5lYXQocGFpbik7XHJcbnBlcnNvbi5lYXQocGFpbik7XHJcbmNvbnNvbGUubG9nKFwiRWxsZSBhIG1hbmfDqVwiLCBwZXJzb24uaHVuZ2VyKTtcclxuXHJcbmxldCBib3VsYW5nZXIgPSBuZXcgQmFrZXIoXCJQYXVsXCIsIDUwMCk7XHJcbmNvbnNvbGUubG9nKFwiUXVhbnRpdMOpIGRlIGZhcmluZVwiLCBib3VsYW5nZXIucXVhbnRpdGVEZUZhcmluZSk7XHJcbmJvdWxhbmdlci5jdWlyZURlc1BhaW5zKDIwMCk7XHJcbmNvbnNvbGUubG9nKFwiUXVhbnRpdMOpIGRlIGZhcmluZVwiLCBib3VsYW5nZXIucXVhbnRpdGVEZUZhcmluZSk7Il0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9