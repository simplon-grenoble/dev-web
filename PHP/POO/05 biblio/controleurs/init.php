<?php
// On charge les classes et les repositories à la demande :
function ChargerClasses($classe){
  if (file_exists("../classes/".$classe.".php")) {
    require "../classes/".$classe.".php";
  } else if (file_exists("../classes/repository/".$classe.".php")) {
    require "../classes/repository/".$classe.".php";
  }
}

// La demande justement :
spl_autoload_register('ChargerClasses');

// On démarre la session :
session_start();
// Code de retour pour dire que la page est trouvée
header("HTTP/1.1 200 found");

require "../config.php";
if ($CONFIG['DATABASE_READY'] === FALSE ) {
  $db = new Database();
  $reponse = $db->initialisationBDD($CONFIG);

  echo $reponse;
}
