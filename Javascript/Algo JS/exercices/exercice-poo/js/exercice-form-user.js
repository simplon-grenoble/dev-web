/**
 * On crée un tableau qui servira à "stocker" les users créés par
 * le formulaire. On le crée ici pour qu'il soit accessible de 
 * manière global et que chaque soumission du formulaire partage
 * le même tableau.
 * (le "stockage" n'est ici que temporaire, on repart à zéro à
 * chaque rechargement de page)
 */

/**
 * Les formulaires ont un event submit qui sera déclencher dès que
 * le formulaire en question est soumit, c'est à dire quand on click
 * sur le bouton submit ou alors que l'on tape enter à l'intérieur
 * d'un des inputs du formulaire
 */

    /**
     * La méthode event.preventDefault() permet d'annuler le comportement
     * par défaut de l'event actuel. Ici, on s'en sert pour faire que
     * la soumission du formulaire n'entraîne pas de rechargement de page
     */
   
    //On récupère toutes les valeurs des inputs du formulaire
    //(manière alternative plus bas)

    //On utilise ces valeurs pour créer une nouvelle instance de User
    //On ajoute cette nouvelle instance dans le tableau créé au début
    
/**
 * La fonction displayUsers a pour but de prendre la list de users
 * et de généré l'affichage à partir de ces users.
 * On a donc la partie visuel de l'appli qui découle de la partie
 * données de l'appli.
 * @param {User[]} userList La liste de user à afficher dans le html
 */