<?php
if (isset($_POST['prenom'])) {
  $_SESSION['Prenom'] = $_POST['prenom'];
}
?>
<div class="contenu">
  <h1>Tableau de bord</h1>

  <h2>Bonjour <?= $_SESSION['Prenom'] ?> !</h2>

  <form action="#" method="POST">
    <label for="prenom">Rentrer votre prénom : </label>
    <input id="prenom" type="text" name="prenom">
    <input type="submit" value="Enregistrer">
  </form>

</div>
