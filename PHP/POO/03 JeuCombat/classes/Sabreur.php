<?php

class Sabreur extends Personnage {

  // Attributs
  //
  // Constantes de classe :
  const FORCE = 4;
  const RESISTANCE = 3;

  public function __construct(Array $donnees)
  {
    parent::__construct($donnees);
  }

  public function attaque($victime, int $nbattaque=0){
    parent::attaque($victime);
    if ($victime->getType() !== "Archer") {
      parent::attaque($victime);
    }
  }

  protected function subirAttaque(int $degats,float $gainResistance,object $attaquant, int $nbattaque=0){

    If($attaquant->getType() == "Archer"){
      $gainResistance = $this->getResistance()*0.2;
      $this->setResistance($gainResistance);
      $this->setVie($degats);
    }else{
      parent::subirAttaque($degats,$gainResistance, $this);
    }
  }

  public function CrierMonType(){
    echo "Je suis un ".get_class()." et je suis le plus doué !!";
  }
}
