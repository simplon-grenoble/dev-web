import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledLink = styled(Link)`
  padding: 15px;
  color: #8186a0;
  text-decoration: none;
  font-size: 18px;
`;

const NavContainer = styled.nav`
  padding: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

function Header() {
  return (
    <NavContainer>
      <div>
        <StyledLink to="/">Accueil</StyledLink>
        <StyledLink to="/recettes">Recettes</StyledLink>
        <StyledLink to="/questions/1">Questions</StyledLink>
      </div>
    </NavContainer>
  );
}

export default Header;
