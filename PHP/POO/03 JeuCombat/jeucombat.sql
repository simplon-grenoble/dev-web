-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 27 jan. 2023 à 12:36
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `jeucombat`
--

-- --------------------------------------------------------

--
-- Structure de la table `personnages`
--

DROP TABLE IF EXISTS `personnages`;
CREATE TABLE IF NOT EXISTS `personnages` (
  `Id_perso` int(11) NOT NULL AUTO_INCREMENT,
  `Nom_perso` varchar(50) NOT NULL,
  `Type_perso` varchar(20) NOT NULL,
  `Vie_perso` int(11) NOT NULL,
  `Force_perso` float DEFAULT NULL,
  `Resistance_perso` float DEFAULT NULL,
  PRIMARY KEY (`Id_perso`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `personnages`
--

INSERT INTO `personnages` (`Id_perso`, `Nom_perso`, `Type_perso`, `Vie_perso`, `Force_perso`, `Resistance_perso`) VALUES
(1, 'Arthur', 'Sabreur', 100, 18, 15.2),(2, 'Merlin', 'Mage', 100, 12, 17),(3, 'Lancelot', 'Brute', 100, 11, 16);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
