// function SimpleTaskManager() {
//   // const task1 = { id: 1, name: "Faire la lessive" };
//   // const task2 = { id: 2, name: "Acheter des légumes" };
//   // const task3 = { id: 3, name: "Appeler le dentiste" };
//   // const tasks = [task1, task2, task3];

//   const tasks = [
//     { id: 1, name: "Faire la lessive" },
//     { id: 2, name: "Acheter des légumes" },
//     { id: 3, name: "Appeler le dentiste" },
//   ];

//   // tasks = nom du tableau & task = nom d'un des éléments du tableau
//   //   const tasksMap = tasks.map((task) => <li>{task.name}</li>);
//   const tasksMap = tasks.map((task) => <Task key={task.id} name={task.name} />);

//   return (
//     <div>
//       <div>SimpleTaskManager</div>
//       <ul>{tasksMap}</ul>
//     </div>
//   );
// }

// function Task(props) {
//   return <li>{props.name}</li>;
// }

function SimpleTaskManager() {
  return (
    <div>
      <div>SimpleTaskManager</div>
      <Task>Faire la lessive</Task>
      <Task>Acheter des légumes</Task>
      <Task>Appeler le dentiste</Task>
    </div>
  );
}

function Task(props) {
  return <div>{props.children}</div>;
}

export default SimpleTaskManager;
