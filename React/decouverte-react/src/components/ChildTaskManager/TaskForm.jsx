import { useState } from "react";

// TaskForm : permet à l'utilisateur de saisir une nouvelle tâche et de l'ajouter à la liste existante.
function TaskForm(props) {
  /***** SI GESTION DU STATE DANS LES ENFANTS 
  Le code de TaskForm reste le même qu'il y ait ou non un lifting state up. Cela est dû au fait que TaskForm ne dépend pas des données de TaskList pour fonctionner. Il se contente simplement de prendre en compte les données du formulaire et de les envoyer à son parent (TaskManager) via la fonction de rappel onTasksChange. *****/
  const [taskTitle, setTaskTitle] = useState("");

  // Lorsque l'utilisateur soumettra le formulaire.
  const handleSubmit = (event) => {
    // On empêche le comportement par défaut du formulaire de recharger la page lorsque l'utilisateur soumet le formulaire.
    event.preventDefault();
    // On crée un nouvel objet newTask qui représente la nouvelle tâche que l'utilisateur a ajoutée. Cet objet possède un id généré à partir de la fonction Date.now(), un title qui correspond au contenu de la variable d'état taskTitle et une valeur completed initialisée à false.
    const newTask = { id: Date.now(), title: taskTitle, completed: false };
    // On appelle la fonction onAddTask passée en prop pour notifier le composant parent (TaskManager) qu'on ajoute la nouvelle tâche newTask à la fin de la liste de tâches existantes.
    props.onAddTask(newTask);
    // On réinitialise la variable d'état taskTitle à une chaîne de caractères vide "".
    setTaskTitle("");
  };

  // Chaque fois que l'utilisateur modifiera le contenu de l'input, on utilise la fonction setTaskTitle pour mettre à jour la variable d'état taskTitle avec la valeur de l'input.
  const handleChange = (event) => {
    setTaskTitle(event.target.value);
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={taskTitle} onChange={handleChange} />
      <button type="submit">Ajouter une tâche</button>
    </form>
  );
}

export default TaskForm;
