// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Strict_mode
"use strict";

/**
 * Ecrire la valeur 'hello' en face de la clé 'test',
 * puis afficher la valeur du localStorage dans la console.
 */

/**
 * Ecrire l'objet suivant dans le localStorage en face de la clé 'myLovelyTrainer',
 * Puis récupérer celle-ci dans le localStorage pour l'afficher dans la console.
 */