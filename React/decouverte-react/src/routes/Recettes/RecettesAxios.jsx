import axios from "axios";
import { useEffect, useReducer } from "react";
import { Loader } from "../../utils/style/Atoms";
import styled from "styled-components";

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const initialState = {
  recettesList: [],
  isDataLoading: false,
  error: false,
  searchValue: "",
};

// Le reducer prend en entrée l'état initial de l'application (state) et une action qui sera dispatchée pour modifier cet état.
function reducer(state, action) {
  // Ce switch  permet de tester différentes valeurs d'une même variable. Ici, on teste la valeur de l'attribut type de l'objet action pour déterminer quelle action a été déclenchée.
  switch (action.type) {
    // Si l'action correspond à l'envoi de la requête, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie les valeurs des clés isDataLoading (pour la passer à true car on est en train de charger) et error (pour la passer à false car la requête est fonctionnelle).
    case "FETCH_RECETTES_REQUEST":
      return {
        ...state,
        isDataLoading: true,
        error: false,
      };
    // Si l'action correspond au succès de la requête, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie les valeurs des clés recettesList (où on passe en props la valeur de la data recettesList reçue) et isDataLoading (pour la passer à false car on est plus en train de charger).
    case "FETCH_RECETTES_SUCCESS":
      return {
        ...state,
        recettesList: action.data.recettesList,
        isDataLoading: false,
      };
    // Si l'action correspond à l'échec de la requête, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie les valeurs des clés isDataLoading (pour la passer à false car on est plus en train de charger) et error (pour la passer à true car la requête a échoué).
    case "FETCH_RECETTES_FAILURE":
      return {
        ...state,
        isDataLoading: false,
        error: true,
      };
    // Si l'action correspond à un changement du contenu du champ de recherche, alors on retourne un nouvel objet qui est une copie de l'état initial (via l'opérateur de décomposition ...state) et on y modifie la valeur de la clé searchValue  (où on passe en props la valeur de la data searchTerm reçue)
    case "SET_SEARCH_VALUE":
      return {
        ...state,
        searchValue: action.data.searchTerm,
      };
    default:
      return state;
  }
}

function Recettes() {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    // Dans le cas où aucune saisie n'est encore faite sur le formulaire
    if (!state.searchValue || state.searchValue === "") {
      dispatch({
        type: "FETCH_RECETTES_SUCCESS",
        data: { recettesList: [] },
      });
      return;
    }

    dispatch({ type: "FETCH_RECETTES_REQUEST" });
    async function fetchAxios() {
      try {
        /* Si on veut faire la requête avec Axios, on peut utiliser le code suivant :

        Axios est une bibliothèque JavaScript populaire qui facilite les requêtes HTTP depuis un navigateur ou un serveur Node.js. Elle offre plusieurs avantages par rapport à l'utilisation de fetch.

        Tout d'abord, Axios est compatible avec tous les navigateurs modernes, y compris les anciennes versions d'Internet Explorer, tandis que fetch est une fonction native des navigateurs modernes et ne fonctionne pas sur les anciennes versions d'Internet Explorer.

        De plus, Axios est plus facile à utiliser et offre des fonctionnalités avancées telles que l'annulation de requêtes, le suivi des progrès de téléchargement et la gestion automatique des cookies. Elle facilite également la manipulation des données de requête et de réponse avec une syntaxe simple et cohérente.

        Enfin, Axios est livré avec un ensemble de plugins qui permettent d'ajouter facilement des fonctionnalités telles que la gestion des erreurs, l'interception des requêtes et des réponses, la transformation des données et bien plus encore.

        https://www.knowledgehut.com/blog/web-development/axios-in-react
        https://www.geeksforgeeks.org/axios-in-react-a-guide-for-beginners/
        https://www.digitalocean.com/community/tutorials/react-axios-react-fr
        https://www.freecodecamp.org/french/news/comment-utiliser-axios-avec-react-le-guide-complet-2021/ */

        // const response = await axios.get(
        //   `https://www.themealdb.com/api/json/v1/1/search.php?s=${state.searchValue}`
        // );
        const response = await axios.get(
          `https://www.themealdb.com/api/json/v1/1/search.php`,
          {
            params: { s: state.searchValue },
          }
        );
        const { meals: recettesList } = response.data;

        // Dans le cas où aucune recette ne correspond
        if (!recettesList) {
          dispatch({
            type: "FETCH_RECETTES_SUCCESS",
            data: { recettesList: [] },
          });
          return;
        }

        dispatch({ type: "FETCH_RECETTES_SUCCESS", data: { recettesList } });
      } catch (error) {
        /* Dans le bloc catch(), nous examinons les différentes propriétés de l'objet error pour déterminer le type d'erreur qui s'est produite et agir en conséquence. Si l'erreur est renvoyée par le serveur, nous pouvons accéder aux données d'erreur via error.response.data et les informations sur la réponse via error.response.status et error.response.headers. Si aucune réponse n'a été reçue du serveur, nous pouvons accéder à error.request. Enfin, si une erreur s'est produite au niveau de la requête elle-même, nous pouvons accéder à error.message. Enfin, error.config contient toutes les informations sur la requête qui a échoué, y compris l'URL, la méthode HTTP et les en-têtes. */
        if (error.response) {
          // Erreur renvoyée par le serveur
          console.log("Data error : ", error.response.data);
          console.log("Status error : ", error.response.status);
          console.log("Headers error : ", error.response.headers);

          return (
            <div>
              <div>{error.message}</div>
              <div>
                <button onClick={handleRetry}>Réessayer</button>
              </div>
            </div>
          );
        } else if (error.request) {
          // Pas de réponse reçue du serveur
          console.log("Request error : ", error.request);
        } else {
          // Erreur au niveau de la requête
          console.log("Erreur", error.message);
        }
        console.log("Config error : ", error.config);

        dispatch({ type: "FETCH_RECETTES_FAILURE" });
      }
    }
    fetchAxios();
  }, [state.searchValue]);

  const handleRetry = (evt) => {
    evt.preventDefault();
    // setSearchValue("");
    // setFormValue("");
    // fetchAxios();
  };

  return (
    <div>
      <h2>Trouvez votre recette</h2>
      <form>
        <input
          type="text"
          value={state.searchValue}
          onChange={(evt) =>
            dispatch({
              type: "SET_SEARCH_VALUE",
              data: { searchTerm: evt.target.value },
            })
          }
          placeholder="Rechercher une recette..."
        />
      </form>
      {state.isDataLoading ? (
        <LoaderWrapper>
          <Loader />
        </LoaderWrapper>
      ) : (
        <ul>
          {state.recettesList.map((recette, index) => (
            <li key={`${recette.idMeal}-${index}`}>
              <h3>{recette.strMeal}</h3>
              <img src={recette.strMealThumb} alt={recette.strMeal} />
              <p>{recette.strInstructions}</p>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default Recettes;
