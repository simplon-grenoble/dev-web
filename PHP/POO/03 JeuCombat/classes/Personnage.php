<?php

abstract class Personnage {
  // Constante de classe :
  const VIE_DEFAUT = 100;
  const GAIN_FORCE_DEFAUT = 0.5;
  const TYPES = ['Mage','Brute','Archer','Sabreur'];

  private static $_compteur = 0;

  // Définir les attributs
  private $_id;
  private $_type;
  private $_nom;
  private $_vie;
  private $_force;
  private $_resistance;

  // Construire l'objet
  public function __construct(Array $donnees)
  {

    $this->hydrate($donnees);

  }

  private function hydrate(Array $valeurs){
    foreach ($valeurs as $key => $valeur) {
      $methode = 'set' . ucfirst($key);
      if (method_exists($this, $methode)) {
        $this->$methode($valeur);
      }
    }
  }

  // Définir les Méthodes
  public function getId(){
    return $this->_id;
  }
  public function getType(){
    return $this->_type;
  }
  public function getNom(){
    return $this->_nom;
  }
  public function getVie(){
    return $this->_vie;
  }
  public function getForce(){
    return $this->_force;
  }
  public function getResistance(){
    return $this->_resistance;
  }

  protected function setId(int $id=NULL){
    if ($id === NULL) {
      $this->_id = self::$_compteur;
    } else {
      $this->_id = $id;
    }
    self::$_compteur ++;
  }

  protected function setType(string $type){
    if (in_array($type,self::TYPES)) {
    $this->_type = $type;
    }
  }

  private function setNom(string $nom){
    $this->_nom = $nom;
  }

  protected function setVie(int $vieDiff){
    if ($this->_vie + $vieDiff <= 0) {
      $this->_vie = 0;
      $this->mort();
    } elseif ($this->_vie + $vieDiff < 100){
      $this->_vie += $vieDiff;
    } else {
      $this->_vie = 100;
    }
  }

  protected function setForce(float $gainForce = self::GAIN_FORCE_DEFAUT){
    if ($this->_force + $gainForce < 0){
      $this->_force = 0;
    } else {
      $this->_force += round($gainForce,2);
    }
  }

  protected function setResistance(float $gainResistance){
    if ($this->_resistance + $gainResistance < 0){
      $this->_resistance = 0;
    } else {
      $this->_resistance += round($gainResistance,2);
    }
  }

  public function attaque(object $victime, int $nbattaque=0){
    $degats = $this->getforce() - $victime->getResistance();
    $gainResistance = 0.1*$degats;
    $degats = $degats < 0 ? 0 : round(-$degats);

    $victime->subirAttaque($degats,$gainResistance, $this, $nbattaque);
    $this->setForce();
  }

  protected function subirAttaque(int $degats,float $gainResistance,object $attaquant, int $nbattaque=0){
    $this->setVie($degats);
    $this->setResistance($gainResistance);
  }

  private function mort(){
    echo $this->_nom." est mort ☠️";
    unset($mort);
  }

  abstract public function CrierMonType();

  final public function Dormir(){

  }
}

