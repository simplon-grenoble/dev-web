export class Bread {

    _weight;
    _type;
    _cooked;

    /*
        @param {number} weight : poids du pain en grammes
        @param {string} type
        @param {boolean} cooked
    */
    constructor(weight, type){
        if(typeof weight !== "number"){
            throw new TypeError("Le poids doit être un nombre");
        }
        if(typeof type !== "string"){
            throw new TypeError("Le type doit être une string");
        }
        this._weight = weight;
        this._type = type;
        this._cooked = false;
    }

    get weight(){
        return this._weight;
    }

    get type(){
        return this._type;
    }

    get cooked(){
        return this._cooked;
    }

    cook(temperature) {
        if(!this._cooked && temperature >= 200 && temperature <= 400){
            this._weight = this._weight * 0.9;
            // this._weight -= this._weight * 0.1;
            // this._weight = this._weight - this._weight * 0.1;
            this._cooked = true;
            return true;
        }
        else {
            return false;
        }
    }

}