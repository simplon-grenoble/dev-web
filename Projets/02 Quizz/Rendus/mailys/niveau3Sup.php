<?php
session_start();
include( 'config.php');
require 'functions.php';
verifConnection();
$score = $_SESSION['score'];
//include('traitement3.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <title>Niveau 3 S</title>
</head>
<body class="bg-image img_monkey2">
    <!-- en tête de la page -->
    <header class="container-fluid bg-primary p-2 ">
        <h1 class="text-light text-center">Niveau 3 Monkey Island</h1>
        <h2> A toi de jouer <?= $_SESSION['username']; ?>!</h2>
        <!-- bouton retour page d'accueil -->
        <form method="POST">
         <button type="submit" class="bg-secondary text-light rounded col-xl-1 col-xs-5 " name="sortir" >sortir </button>   
        </form>
    </header>
    <form method="POST">        
    </form>
   
   <div class="etoile">
           <div class="content">    
               <h2>score:</h2>
               <p><?= $_SESSION['score'] ?> points</p>
           </div>
       </div>
       <script src="assets/script/javascript.js"> </script>
</body>
</html>