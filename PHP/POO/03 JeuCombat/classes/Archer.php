<?php

class Archer extends Personnage {

  // Attributs
  // Constantes de classe :
  const FORCE = 2;
  const RESISTANCE = 2;


  public function __construct(Array $donnees)
  {
    parent::__construct($donnees);
  }

  public function attaque($victime, int $nbattaque=0){
    if ($victime->getType() !== "Brute") {
      parent::attaque($victime);
    } else {
      $degats = $this->getforce() - $victime->getResistance() - $this->getForce()*0.1;
      $gainResistance = 0.1*$degats;
      $degats = $degats < 0 ? 0 : round(-$degats);

      $victime->subirAttaque($degats,$gainResistance, $this);
      $this->setForce();
    }
  }

  protected function subirAttaque(int $degats,float $gainResistance,object $attaquant, int $nbattaque=0){

    If($attaquant->getType() == "Sabreur"){
      $gainResistance = 0;
      $this->setResistance($gainResistance);
      $this->setVie($degats);
    }else{
      parent::subirAttaque($degats,$gainResistance, $this);
    }

  }

  public function CrierMonType(){
    echo "Je suis un ".get_class()." et je suis le plus précis !!";
  }
}
