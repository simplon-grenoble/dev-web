"use strict";

/**
 * Le `localstorage` est une zone du navigateur NON SECURISEE
 * dans laquelle nous pouvons enregistrer des informations avec un système clé/valeur.
 * 
 * Pour écrire dans le `localStorage`, on peut utiliser la méthode `setItem()` sur l'objet (déjà existant) `localStorage`.
 * `setItem()` attend deux paramètres, la clé dans la quelle on souhaite stocker nos données et
 * la valeur que l'on souhaite stocker. Le localStorage n'accepte pas les objets comme valeur.
 * 
 * La méthode `getItem()` permet de récupérer une valeur qui se trouve en face de la clé passée en paramètres.
 */

/**
 * Le localStorage n'acceptant pas les objets,
 * il est nécessaire de les encoder en JSON et ainsi d'en faire une string avant d'enregistrer celui-ci.
 */

/**
 * inversement, pour pouvoir récupérer la valeur d'un objet JSON dans le localStorage, il ne faut pas oublier de parser celui-ci.
 */