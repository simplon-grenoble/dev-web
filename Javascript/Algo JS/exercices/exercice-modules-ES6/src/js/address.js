/**
 * Pour pouvoir importer quelque chose depuis un autre fichier/module
 * il faut que ce fichier indique ce qu'il rend accessible à l'extérieur avec le mot clef export.
 * Ici, on indique qu'on export notre classe Address, donc que celle ci sera importable par d'autres fichiers
 * (on peut export des variables, des fonctions et des classes).
 * Tout ce qui n'est pas exporté ne sera pas accessible hors du fichier où l'on se trouve.
 */