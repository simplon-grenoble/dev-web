<?php
session_start();
require "Structures/header.php"
?>

<main class="index">
    <form action="#" method="GET">
        <label for ="pseudo">Entre un pseudo</label>
        <input type="text" name="pseudo" placeholder="Renseignez votre pseudonyme" required>
        <input type="submit" value="Connecter" class="btn">
    </form>
</main>
<?php if (isset($_GET["pseudo"])) {
    header("location:Q1.php");
} ?>

<?php $_SESSION["pseudo"] = is_set($_GET, "pseudo") ?>
<?php require "Structures/footer.php" ?>
