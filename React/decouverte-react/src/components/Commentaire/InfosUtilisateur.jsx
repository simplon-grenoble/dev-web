import React from "react";
import Avatar from "./Avatar";

class InfosUtilisateur extends React.Component {
  render() {
    /* Lorsque vous utilisez des propriétés dans votre composant, 
    vous pouvez les extraire en utilisant des accolades pour accéder à leur valeur. 
    Cela s'appelle la "déstructuration" d'objet en JavaScript.*/
    const { utilisateur } = this.props;

    function formaterNom(user) {
      return user.firstName + " " + user.lastName;
    }

    return (
      <div className="InfosUtilisateur">
        <Avatar utilisateur={utilisateur} />
        <h1>{formaterNom(utilisateur)}</h1>
      </div>
    );
  }
}

export default InfosUtilisateur;
