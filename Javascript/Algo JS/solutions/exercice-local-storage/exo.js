// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Strict_mode
"use strict";

/**
 * Ecrire la valeur 'hello' en face de la clé 'test',
 * puis afficher la valeur du localStorage dans la console.
 */
localStorage.setItem('test','hello');
console.log(localStorage.getItem('test'));

/**
 * Ecrire l'objet suivant dans le localStorage en face de la clé 'myLovelyTrainer',
 * Puis récupérer celle-ci dans le localStorage pour l'afficher dans la console.
 */
let myLovelyTrainer = {
  name: 'Mathieu',
  surname: 'Morel',
  birthdate: '15/07/1989'
}

let jsonToLocalStorage = JSON.stringify(myLovelyTrainer);
localStorage.setItem('myLovelyTrainer',jsonToLocalStorage);

let jsonFromLocalstorage = localStorage.getItem('myLovelyTrainer');
let trainer = JSON.parse(jsonFromLocalstorage);
console.log(trainer);