/**
 * On crée un tableau qui servira à "stocker" les users créés par
 * le formulaire. On le crée ici pour qu'il soit accessible de 
 * manière global et que chaque soumission du formulaire partage
 * le même tableau.
 * (le "stockage" n'est ici que temporaire, on repart à zéro à
 * chaque rechargement de page)
 */
let listeUtilisateurs = [];

/**
 * Les formulaires ont un event submit qui sera déclenché dès que
 * le formulaire en question est soumit, c'est à dire quand on click
 * sur le bouton submit ou alors que l'on tape enter à l'intérieur
 * d'un des inputs du formulaire
 */
// https://developer.mozilla.org/fr/docs/Learn/Forms/Form_validation
let form = document.querySelector("#form-user");
form.addEventListener("submit", onFormValidated, false);

let showButton = document.querySelector("#show");
showButton.addEventListener("click", onShowButtonClicked);
//showButton.addEventListener("click", showUsers);

function onShowButtonClicked(event){
    showUsers();
}

function onFormValidated(event){
    // Et on empêche l'envoi des données du formulaire
    // https://developer.mozilla.org/fr/docs/Web/API/Event/preventDefault
    /**
     * La méthode event.preventDefault() permet d'annuler le comportement
     * par défaut de l'event actuel. Ici, on s'en sert pour faire que
     * la soumission du formulaire n'entraîne pas de rechargement de page
     */
    event.preventDefault();

    // On récupère toutes les valeurs des inputs du formulaire
    let valeurDuNom = document.querySelector("#name").value;
    let valeurDuMail = document.querySelector("#mail").value;
    let valeurDuMotDePasse = document.querySelector("#password").value;
    let valeurDeLaDateDeNaissance = document.querySelector("#birthdate").value;
    let valeurDeLavatar = document.querySelector("#avatar").value;

    // On utilise ces valeurs pour créer une nouvelle instance de User
    let utilisateur = new User(valeurDuNom, valeurDuMail, valeurDuMotDePasse, new Date(valeurDeLaDateDeNaissance), valeurDeLavatar);
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/push
    // On ajoute cette nouvelle instance dans le tableau créé au début

    listeUtilisateurs.push(utilisateur);
}


/**
 * La fonction showUsers a pour but de prendre la list de users
 * et de généré l'affichage à partir de ces users.
 * On a donc la partie visuel de l'appli qui découle de la partie
 * données de l'appli.
 * @param {User[]} userList La liste de user à afficher dans le html
 */
function showUsers(){
    let affichageListeUtilisateurs = document.querySelector("#user-list");
    affichageListeUtilisateurs.innerHTML = '';

    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
    //listeUtilisateurs.forEach(element => console.log(element));
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Statements/for...of
    for(let utilisateur of listeUtilisateurs) {
        // https://fr.javascript.info/modifying-document
        let affichageHTML = utilisateur.toHTML();
        affichageListeUtilisateurs.append(affichageHTML);
    }
}