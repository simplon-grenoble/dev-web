# Exercices POO JS

## Mini exercice object : (js/exercice-objet.js)
Faire l'objet stylo avec les propriétés color, type, size et une methode write, et faire que la méthode write ajoute un élément dans le html.
1) Créer les fichier exercice-object.html/js.
2) Créer un objet avec les accolades comme dans l'exemple et mettre les différentes propriétés avec les valeurs que vous souhaités (le size devra être du number).
3) Rajouter une propriété write qui sera une méthode (une fonction dans un objet), et faire que cette méthode crée un élément html et mette en textContent de cet élément 'bloup' avant de le mettre dans le body.

## Mini exercice : (js/user.js)
Faire une classe user avec comme propriétés un name, un mail, un password, une birthdate et un avatar.

## Suite exercice : (js/exercice-form-user.js)
Faire un formulaire d'inscription html qui, lorsqu'on le submit, va créer une nouvelle instance de la classe User avec comme valeurs, celles entrées dans le formulaire.

1) Créer un fichier exercice-form-user.js et un fichier html dans lequel on chargera les deux scripts utilisés (user.js et celui qu'on vient de créer).
2) Faire un formulaire html dans le fichier html avec les inputs nécessaire pour faire un user (name,mail,password,birthdate,avatar).
3) Dans le javascript, ajouter un event sur le bouton qui ira récupérer les valeurs des différents inputs (les afficher en console, voir si ça marche).
4) Dans cet event, faire que l'on utilise ces valeurs pour créer une instance de la classe User (et afficher celle ci en console).

## Mini exercice : (js/exercice-form-user.js / js/user.js)
En se basant sur l'exercice d'hier, récupérer l'instance de user qu'on a créée et la stocker dans un tableau (hors de l'event listener).
Ensuite, créer une méthode toHTML dans la classe User qui va créer un élément HTML avec les différentes informations du user dedans et le return.
1) Dans le fichier exercice-user-form.js créer une variable list avec un tableau vide dedans.
2) Dans l'eventListener du form, faire qu'une fois qu'on a créé l'instance, on la push dans le tableau.
3) Dans le fichier user.js ajouter une nouvelle méthode toHTML().
4) Dans cette méthode, créer un nouvel élément avec document createElement et faite la structure que vous voulez pour le user (exemple de structure : article > 1 paragraphe par propriété + une img pour l'avatar).
5) Faire que la méthode return l'élément HTML créé.

## Exercice form to instance : (js/dog.js - js/exercice-form-dog.js)
Dans de nouveaux fichiers, faire une nouvelle classe Dog (ou Cat, ou s'que vous voulez dans l'absolu) qui aura par exemple comme propriété un name en string, une race en string, une couleur en string et un age en number. Puis, comme on l'a fait précédemment, faire un formulaire HTML qui permettra de créer des nouvelles instances de cette classe, qu'on stockera dans un tableau.

I. La classe Dog.
1) Faire un nouveau fichier js/dog.js.
2) Créer une classe avec un constructor permettant d'assigner les propriétés name/race/age/color au Dog.

II. Le formulaire HTML
3) Faire un fichier exercice-form-dog.html.
4) Créer dedans un formulaire HTML (bootstrap ou non, osef) avec les 3 inputs.

III. Le traitement du formulaire
5) Faire un fichier exercice-form-dog.js.
6) Dans ce fichier, créer un tableau vide qui contiendra nos chiens.
7) Puis mettre un event listener sur le formulaire qui récupérera les valeurs des inputs.
8) Utiliser les valeurs de ces inputs pour créer une nouvelle instance de Dog.
9) Mettre la nouvelle instance dans le tableau créé précédemment.