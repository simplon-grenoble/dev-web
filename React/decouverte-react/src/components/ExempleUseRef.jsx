import { useState } from "react";
import { useRef } from "react";

// C'est une erreur de croire que les states sont la seule manière de garder en mémoire une donnée entre chaque changement d’état. Il existe un autre élément de React qui est persistant malgré les changement d’états, c’est les références. Elles permettent en plus de garder en mémoire des variables que l’on souhaite modifier sans pour autant vouloir re-render le composant.

// useRef est un Hook de React qui permet de stocker une référence mutable vers une valeur dans un composant fonctionnel. Cette référence persiste entre les rendus du composant, contrairement aux variables locales du composant qui sont recréées à chaque rendu.

function generateName() {
  return new Promise((resolve) => {
    setTimeout(() => {
      const names = [
        "Kayila",
        "Lucas",
        "Mailys",
        "Alexandre",
        "Christian",
        "Gaël",
        "Mickael",
        "Boubacar",
        "Olivier",
        "Aleksandra",
        "Linda",
        "Sélim",
      ];
      const randomIndex = Math.floor(Math.random() * names.length);
      resolve(names[randomIndex]);
    }, 1000);
  });
}

// En résumé, useRef permet ici de stocker une valeur persistante entre les rendus du composant et de garder une trace du nombre de clics effectués sur le composant.
const ExempleUseRef = () => {
  const [object, setObject] = useState({
    name: "Kayila",
    click: 0,
  });
  const nombreDeClique = useRef(0);

  const handleClick = () => {
    // La référence nombreDeClique est initialisée à 0 à l'aide de useRef(0). Elle est ensuite utilisée dans la fonction handleClick pour compter le nombre de clics sur le composant. À chaque clic, la valeur de la référence est incrémentée, et si le nombre de clics est un multiple de 10, une nouvelle valeur de nom est générée à l'aide de la fonction generateName et mise à jour dans l'état du composant à l'aide de setObject.
    nombreDeClique.current++;
    if (nombreDeClique.current % 10 === 0) {
      generateName().then((newName) => {
        setObject({ ...object, name: newName });
      });
    }
  };

  return <div onClick={handleClick}>{object.name}</div>;
};

export default ExempleUseRef;
