// Cacher les question
var lequiz11 = document.querySelector('#eleventh_li').style.display = "none";
var lequiz12 = document.querySelector('#twelfth_li').style.display = "none";
var lequiz13 = document.querySelector('#thirteenth_li').style.display = "none";
var lequiz14 = document.querySelector('#fourteenth_li').style.display = "none";
var lequiz15 = document.querySelector('#fifteenth_li').style.display = "none";

// Cacher les boutons
var hideButton11 = document.querySelector("#btnQuest12").style.display = "none";
var hideButton12 = document.querySelector("#btnQuest13").style.display = "none";
var hideButton13 = document.querySelector("#btnQuest14").style.display = "none";
var hideButton14 = document.querySelector("#btnQuest15").style.display = "none";

var submit0 = document.querySelector("#SUBMIT").style.display = "none";



// La fonction est appeler par le bouton "start_game" et demande de la fonction 'btnQuest6()' => d'afficher la première question et le bouton 'btnQuest6'
// => et de cacher le bouton 'start_game2'
function question_11() {
    // Caché
    var hideButton = document.querySelector("#start_game3").style.display = "none";
    // Visible
    hideButton11 = document.querySelector("#btnQuest12").style.display = "block";
    lequiz11 = document.querySelector('#eleventh_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest6" et demande de la fonction 'btnQuest7()' => d'afficher la deuxième question et le bouton 'btnQuest7'
// => et de cacher le bouton 'btnQuest6' et la première question
function question_12() {
    // Caché
    hideButton11 = document.querySelector("#btnQuest12").style.display = "none";
    lequiz11 = document.querySelector('#eleventh_li').style.display = "none";
    // Visible
    hideButton12 = document.querySelector("#btnQuest13").style.display = "block";
    lequiz12 = document.querySelector('#twelfth_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest7" et demande de la fonction 'btnQuest8()' => d'afficher la troixième question et le bouton 'btnQuest8'
// => et de cacher le bouton 'btnQuest7' et la deuxième question
function question_13() {
    // Caché
    hideButton12 = document.querySelector("#btnQuest13").style.display = "none";
    lequiz12 = document.querySelector('#twelfth_li').style.display = "none";
    // Visible
    hideButton13 = document.querySelector("#btnQuest14").style.display = "block";
    lequiz13 = document.querySelector('#thirteenth_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest8" et demande de la fonction 'btnQuest9()' => d'afficher la quatrième question et le bouton 'btnQuest9'
// => et de cacher le bouton 'btnQuest8' et la troixième question
function question_14() {
    // Caché
    hideButton13 = document.querySelector("#btnQuest14").style.display = "none";
    lequiz13 = document.querySelector('#thirteenth_li').style.display = "none";
    // Visible
    hideButton14 = document.querySelector("#btnQuest15").style.display = "block";
    lequiz14 = document.querySelector('#fourteenth_li').style.display = "block";
}

// La fonction est appeler par le bouton "btnQuest9" et demande de la fonction 'btnQuest10()' => d'afficher la cinqième question et le bouton 'SUBMIT'
// => et de cacher le bouton 'btnQuest9' et la quartième question
function question_15() {
    // Caché
    hideButton14 = document.querySelector("#btnQuest15").style.display = "none";
    lequiz14 = document.querySelector('#fourteenth_li').style.display = "none";
    // Visible
    lequiz15 = document.querySelector('#fifteenth_li').style.display = "block";
    submit0 = document.querySelector("#SUBMIT").style.display = "block";
}

// Jusqu'à là c'est bon mais ce n'est pas "DRY" !!!-