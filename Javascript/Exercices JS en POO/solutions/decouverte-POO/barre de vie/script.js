// Créer une classe Barre de vie
class BarreDeVie {
  _vie;
  _couleur;
  // Si on nous passe les paramètres vie et couleur, on les utilise, sinon on prend les paramètres par défaut.
  constructor(vie = 100,couleur = "green"){
    // On vérifie aussi que vie est comprise entre 0 et 100.
    if (vie <= 100 && vie >= 0) {
      this._vie = vie;
      this._couleur = couleur;
    }else{
      // Exemple d'erreur personnalisée : throw permet d'écrire du texte lors d'une erreur, et de stopper l'execution du code.
      throw('la vie doit être comprise entre 0 et 100')
    }

    // On récupère les éléments HTML
    let conteneur = document.querySelector('#conteneur');
    let barredevie = document.querySelector('#barredevie');
    conteneur.style.border = "2px solid black";

    // On crée notre élément dans le HTML
    creationBarre(this._vie,this._couleur);
  }

  // getter qui permet d'afficher la vie.
  get vie(){
    return this._vie;
  }

  // Méthode qui permet de rajouter de la vie.
  // Si la vie actuelle plus la vie ajoutée est inférieur à 100, on additionne la vie actuelle et la vie ajoutée. si elle dépasse 100, on met la vie à 100 (on ne peut pas aller au-dessus).
  // Puis on redessine la barre.
  gagnerVie(vie){
    if (this._vie + vie <= 100) {
      this._vie += vie;
    }else{
      this._vie = 100;
    }
    creationBarre(this._vie,this._couleur)

  }

  // Méthode sous forme de setter pour enlever de la vie.
  // Fonctionne comme gagnerVie.
  // Dans le cas où on atteint zéro, on appelle la fonction mort.
  set perdreVie(vie){
    if (this._vie - vie > 0) {
      this._vie -= vie;
      creationBarre(this._vie,this._couleur)
    }else{
      this._vie = 0;
      mort();
    }

  }

}

// Permet de créer l'élement visuel.
function creationBarre(vie,couleur){
  conteneur.innerHTML = "<div id=\"barredevie\" style=\"background-color:"+couleur+";width:"+vie+"%; height:20px; text-align:center; color:#FFF;\"></div>"
  barredevie.innerHTML = vie+"%";
}

//Dans le cas où on meurt, on crée l'élément visuel, et on ajoute un texte lié.
function mort(){
  creationBarre(100,'red');
  barredevie.innerHTML = "Tu es mort";
}

// On instancie ensuite notre classe.
let barre = new BarreDeVie;


// Manière de rendre le code dynamique avec des addEventListener.
// On commence par récupérer les boutons.
let moins = document.getElementById('moins');
let plus = document.getElementById('plus');

// Puis on ajoute un écouteur d'évènement :
// lorsqu'on réalise l'action "click", sur le bouton moins,
// on réalise le code dans la fonction anonyme.
moins.addEventListener("click",function(){ barre.perdreVie = 10 })

// Autre moyen :
// Lorsqu'on réalise l'action "click", sur le bouton plus,
// on réalise la fonction PlusVie, définie plus bas.
// Attention, on ne met pas de aprenthèses après la fonction !!!
plus.addEventListener("click",plusVie);

function plusVie(){
  barre.gagnerVie(10)
}
