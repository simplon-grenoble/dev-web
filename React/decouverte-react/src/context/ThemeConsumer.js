import { useContext } from "react";
import { ThemeContext } from "./ThemeContext";

function ThemeConsumer() {
  // ATTENTION pas de crochets comme dans useState mais des accolades : const [theme, setTheme] c'est pas bon !
  const { theme, toggleTheme } = useContext(ThemeContext);

  return (
    <div>
      <button onClick={toggleTheme}>Toggle theme</button>
      <p>Current theme: {theme}</p>
    </div>
  );
}

export default ThemeConsumer;
