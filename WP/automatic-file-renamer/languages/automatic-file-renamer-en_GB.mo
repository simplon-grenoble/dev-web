��          �   %   �      p  
   q     |     �  3   �     �  ^   �  d   T  8   �  !   �  E     Z   Z     �     �  .   �  V   �  �   U  �   )     �     �  @   �  (   $     M  (   e  r   �  	               �  )  
   �	     �	     �	  0   �	     
  \   #
  T   �
  .   �
       3     M   O     �     �  #   �  >   �  �     �   �     l     ~  C   �  #   �     �        W   3  	   �  	   �     �           
                                                                                            	                    .extension Activer le renommage Activer les redirections Actuellement, vos fichiers seront renommés ainsi : Automatic File Renamer Ce nombre varie. il permet de différencier deux enregistrements avec le même nom de fichier. Ce réglage s'appliquera à tous les fichiers qui seront téléversés.<br>Ce n'est pas rétroactif. Cela peut être très embêtant, pour pleins de raisons  Choisissez des termes évocateurs Choisissez quel(s) role(s) peuvent modifier les réglages ci-dessus : D'autant plus qu'une fois que la page est créée, il est très dangereux de la renommer ! Documention Enregistré ! La redirection permet de cacher la page liée. Les moteurs de recherche (et vous aussi) aiment quand les mots décrivent le contenu ! Lors du téléversement d'un média, <b>une page liée est automatiquement créée</b>.<br>
  <p>Si votre fichier a pour nom "<b>cheval.jpg</b>", la page s'appelera "<b>https://votresite.fr/cheval</b>".
    <br> Pour éviter cela, il est possible grâce à ce plugin de renommer automatiquement vos fichiers et leur pages liées lors de leur téléversement. Redirection activée Renommage activé Renvoie sur la page attachée, si elle existe (sinon erreur 404) Renvoie sur le fichier média (défaut). Renvoie une erreur 404. Réglages réservés aux administrateurs Une fois enregistré, le nom du fichier n'est plus modifiable. Choisissez le nom AVANT de téléverser le fichier. extension nom-du-fichier évoquées ici Project-Id-Version: Automatic-File-Renamer
PO-Revision-Date: 2022-02-24 10:58+0100
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ../Admin
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: AFR-admin-Panel.php
 .extension Enable renaming Enable redirects Currently,your files will be renamed like this : Automatic File Renamer This number changes. it allows to differentiate between two records with the same file name. This setting will apply to all files that will be uploaded.<br>It's not retroactive. That can be really annoying, for many reasons  Choose evocative terms Choose wich role(s) can change the above settings : Especially since once the page is created, it's very dangerous to rename it ! Documention Saved ! The redirect hides the linked page. Browsers (and you too) love when words describe file content ! When a media is uploaded, <b>an attachment page is automatically created</b>.<br>
  <p> If your file's name is "<b>horse.jpg</b>", the attachment page will be called "<b>https://your-site.com/horse</b>".
    <br> To avoid this, it's possible thanks to this plugin to rename automatically your files and their attachment pages while the upload. Redirects enabled Renaming enabled Redirect to the attachment page, if it exists (or else, 404 error). Redirect to the file url (default). Redirect to your 404 page. Settings only for Administrators Once saved, the file name cannot be changed. Choose the name BEFORE uploading the file. extension file-name mentioned here 