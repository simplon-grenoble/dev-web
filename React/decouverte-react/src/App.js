import "./App.css";
import React from "react";
import Commentaire from "./components/Commentaire/";
import Counter from "./components/CounterExampleUseEffect";
import ExemplePropType from "./components/ExemplePropType";
import Modal from "./components/Modal";
import { ThemeProvider } from "./context/ThemeContext";
import ThemeConsumer from "./context/ThemeConsumer";
import SimpleTaskManager from "./components/SimpleTaskManager";
import ParentTaskManager from "./components/ParentTaskManager";
import ChildTaskManager from "./components/ChildTaskManager";
import ExempleUseReducer from "./components/ExempleUseReducer";
import ExempleUseRef from "./components/ExempleUseRef";
// import List from "./components/List";

// const name = "Charlie Chaplin";
// const monElementJSX = <h1>Bonjour, {name}</h1>;

// const utilisateur = {
//   firstName: "Audrey",
//   lastName: "Hepburn",
//   avatarUrl:
//     "https://artmiens.files.wordpress.com/2018/03/audrey-hepburn-26.jpg",
// };

// function direBonjour(user) {
//   if (user) {
//     return <h1>Bonjour, {user.prenom + " " + user.nom} !</h1>;
//   }
//   return <h1>Bonjour, Belle Inconnue.</h1>;
// }

// function ComposantWelcome(props) {
//   return direBonjour(props.user);
//   // return <h1>Bonjour, {direBonjour(props.user)}</h1>;
//   // return <h1>Bonjour, {props.prenom + " " + props.nom}</h1>;
// }

// const items = [
//   { id: 1, text: "Acheter du lait" },
//   { id: 2, text: "Sortir le chien" },
//   { id: 3, text: "Aller à la salle de sport" },
// ];

class App extends React.Component {
  render() {
    return (
      <div class="MesComposants">
        {/* <ComposantWelcome
          prenom={utilisateur.firstName}
          nom={utilisateur.lastName}
        />
        <ComposantWelcome user={utilisateur} /> */}
        <Commentaire />
        <Counter />
        <ExemplePropType name="Arthur" age={26} />
        <Modal />
        <SimpleTaskManager />
        <ParentTaskManager />
        <ChildTaskManager />
        <ThemeProvider>
          <ThemeConsumer />
          <div>
            <h1>Hello World</h1>
            <p>
              This is an example of changing background color with theme toggle
            </p>
          </div>
        </ThemeProvider>
        <ExempleUseReducer />
        <ExempleUseRef />
        {/* <List items={items} /> */}
      </div>
      // <div className="App">
      //   {/* <header className="App-header">{monElementJSX}</header> */}
      //   <header className="App-header">{direBonjour(utilisateur)}</header>
      //   <ComposantWelcome name="Olivier" />
      // </div>
    );
  }
}

export default App;
