<?php
session_start();



if (isset($_POST['input'])) {
    if ($_POST['input'] === 'Amplificatum') {
        header('Location: cinemas3.php');
        exit;
    } else if ($_POST['input'] === 'Locomotor') {
        $_SESSION['scores']++;
        header('Location: cinemas3.php');
        exit;
    } else if ($_POST['input'] === 'Enervatum') {
        header('Location: cinemas3.php');
        exit;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>quiz</title>
</head>
<body id="corn" class="p2">
    <img class="pop" src="src/pop.png">
    <video id="ecran" autoplay controls>
        <source src="src/harry.mp4" type="video/mp4">
    </video>
    <p class="txthp">Complêtez la formule de Minerva Mcgonagall : Piertotum ... ?</p>
    <form method="post">
        <input class="cinema1" type="submit" name="input" value="Amplificatum">
        <input class="cinema1" type="submit" name="input" value="Locomotor">
        <input class="cinema1" type="submit" name="input" value="Enervatum">
    </form>
</body>
</html>