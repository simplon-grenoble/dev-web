import { useState } from "react";
import { useQuery } from "react-query";
import { Loader } from "../../utils/style/Atoms";
import styled from "styled-components";

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

function Recettes() {
  // Crée un état local pour stocker la valeur de l'input du formulaire de recherche. La valeur initiale est une chaîne de caractères vide, ce qui signifie que le formulaire sera vide au chargement initial de la page, ainsi qu'un deuxième état pour stocker la valeur totale du mot cherché une fois le formulaire validé.
  const [searchValue, setSearchValue] = useState("");
  const [formValue, setFormValue] = useState("");

  const fetchData = async () => {
    // Si la valeur de l'input de recherche est vide ou nulle, la fonction renvoie un objet avec une propriété recettesList qui est un tableau vide. Cela signifie qu'aucune recette ne sera affichée si l'utilisateur n'a rien saisi dans le formulaire.
    if (!formValue || formValue === "") {
      return { recettesList: [] };
    }

    const response = await fetch(
      `https://www.themealdb.com/api/json/v1/1/search.php?s=${formValue}`
    );

    const { meals } = await response.json();

    // Si l'objet meals est nul ou vide, ce qui signifie qu'aucune recette n'a été trouvée. Si c'est le cas, la fonction lance une erreur qui sera gérée plus tard dans le code.
    if (!meals) {
      throw new Error("Aucune recette trouvée. Veuillez réessayer.");
    }

    return { recettesList: meals };
  };

  // La fonction useQuery fournie par la bibliothèque react-query pour gérer les requêtes de données asynchrones. Cette fonction prend deux arguments: un tableau d'identifiants pour la requête (dans ce cas, le tableau [ "recettes", searchValue ]), et une fonction asynchrone qui renvoie les données demandées.
  const { data, error, isError, isLoading, refetch } = useQuery(
    ["recettes", formValue],
    fetchData
  );

  const handleRetry = (evt) => {
    evt.preventDefault();
    setSearchValue("");
    setFormValue("");
    refetch();
  };

  const handleInputChange = (evt) => {
    evt.preventDefault();
    setSearchValue(evt.target.value);
  };

  const handleFormSubmit = (evt) => {
    evt.preventDefault();
    setFormValue(searchValue);
  };

  if (isLoading) {
    return (
      <LoaderWrapper>
        <Loader />
      </LoaderWrapper>
    );
  }

  return (
    <div>
      <h2>Trouvez votre recette</h2>
      {isError ? (
        <div>
          <div>{error.message}</div>
          <div>
            <button onClick={handleRetry}>Réessayer</button>
          </div>
        </div>
      ) : (
        <div>
          <form>
            <input
              id="searchValue"
              type="text"
              value={searchValue}
              onChange={handleInputChange}
              placeholder="Rechercher une recette..."
            />
            <button onClick={handleFormSubmit}>Envoyer</button>
          </form>
          <ul>
            {data.recettesList.map((recette, index) => (
              <li key={`${recette.idMeal}-${index}`}>
                <h3>{recette.strMeal}</h3>
                <img src={recette.strMealThumb} alt={recette.strMeal} />
                <p>{recette.strInstructions}</p>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default Recettes;
