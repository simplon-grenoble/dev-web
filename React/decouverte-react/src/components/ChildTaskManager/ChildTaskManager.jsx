import { useState } from "react";
import TaskList from "./TaskList";

/* Chaque composant TaskList et TaskForm maintient son propre état tasks et appelle une fonction de rappel (onTasksChange) fournie en tant que prop pour signaler tout changement de tâches à son parent TaskManager.
Cependant, cet état est en réalité partagé entre les deux composants, ce qui peut entraîner des erreurs de synchronisation et une complexité accrue. */

function ChildStateTaskManager() {
  const [tasks, setTasks] = useState([
    { id: 1, title: "Faire les courses", completed: false },
    { id: 2, title: "Sortir le chien", completed: true },
    { id: 3, title: "Apprendre React", completed: false },
  ]);

  return (
    <div>
      {/***** SI GESTION DU STATE DANS LES ENFANTS
      On peut voir que dans TaskList, l'état tasks est initialisé avec la valeur de props.tasks, et la fonction setTasks est utilisée pour mettre à jour l'état. Cela permet de garder la gestion de l'état dans TaskList plutôt que de la déplacer vers TaskManager. ******/}
      <TaskList tasks={tasks} />
    </div>
  );
}

export default ChildStateTaskManager;
