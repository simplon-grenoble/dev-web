import React from "react";

class Avatar extends React.Component {
  render() {
    /* Lorsque vous utilisez des propriétés dans votre composant, 
    vous pouvez les extraire en utilisant des accolades pour accéder à leur valeur. 
    Cela s'appelle la "déstructuration" d'objet en JavaScript.*/
    const { utilisateur } = this.props;

    return (
      <img
        class="imageUtilisateur"
        src={utilisateur.avatarUrl}
        alt={utilisateur.firstName}
      ></img>
    );
  }
}

export default Avatar;
