<?php
class Test {
  static public function a(){
    echo "je suis dans le a de test <br>";

    static::b();

  }
  static public function b(){
    echo "je suis dans le B de Test <br>";
  }
}

class TestFille extends Test {
  static public function b(){
    echo "Je suis dans le B de FilleTest <br>";

    static::c();

  }

  protected function c(){
    echo "Méthode c de FilleTest <br>";
  }
}

class FilleFille extends TestFille {

   protected function c(){
    echo "Méthode c de FilleFille <br>";
  }
}

Test::a();
TestFille::a();
FilleFille::a();
