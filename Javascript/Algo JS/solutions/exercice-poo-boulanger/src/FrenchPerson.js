export class FrenchPerson{

    _hunger;
    _name;

     /*
        @param {number} hunger : faim de la personne
        @param {string} name : nom de la personne
    */
    constructor(hunger, name){
        this._hunger = hunger;
        this._name = name;
    }

    get hunger(){
        return this._hunger;
    }

    get name(){
        return this._name;
    }

    eat(pain){
        if(this._hunger > 0){
            this._hunger -= pain.weight;
        }            
    }

}