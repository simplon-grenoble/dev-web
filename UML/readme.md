Du UML en veux tu en voilà, on vous conseille FORTEMENT de faire à minima un diagramme user case, voir un diagramme de séquence dans votre dossier de projet :
- https://creately.com/blog/fr/uncategorized-fr/tutoriel-sur-les-diagrammes-de-classe/
- https://creately.com/blog/fr/diagrammes/tutoriel-sur-les-diagrammes-de-cas-dutilisation/
- https://creately.com/blog/fr/diagrammes/tutoriel-sur-le-diagramme-de-sequence/
- https://creately.com/blog/fr/uncategorized-fr/tutorial-diagrammes-de-activite-uml/

Un très bon cours, à lire absolument !!!
https://lipn.univ-paris13.fr/~gerard/uml-s2/uml-cours00.html

Les outils online pour créer ces schémas :
- https://creately.com/diagram-type/use-case/
- https://staruml.io/download
- https://www.diagrams.net/
- https://app.diagrams.net/

D'autres cours :
- http://remy-manu.no-ip.biz/UML/Cours/coursUML11.pdf
- http://remy-manu.no-ip.biz/UML/Cours/coursUML5.pdf

https://drive.google.com/file/d/1JGrsFKZIwky8PlNQR18wYKvX7RovFWLg/view?usp=share_link le dossier de Jordy contient tous les diagrammes nécessaires :
- Diagramme de base de données
- Diagramme de classes
- Diagramme d’activité
- Diagramme de séquence
- Diagramme use case
