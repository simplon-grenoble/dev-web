<?php
session_start();

require 'functions.php';
// initialisation du pseudo pour la session et envois sur quizz si pseudo valide    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/style.css">

    <title>Accueil</title>
</head>
<body class="bg-image img_monkey1">
    <!-- en tête de la page d'accueil -->
    <header class="container-fluid bg-primary p-2">
        <h1 class="text-light text-center">Quizz Monkey Island</h1>
    </header>

        <div class="container bg-tercery col-xl-6 col-sm-9 text-center p-5 m-auto mt-5 rounded-5">
            <h2>Bienvenu dans le monde piratesque de Monkey Island!</h2>
            <p>Si vous voulez tester votre connaissance sur l’univers de Monkey Island Entrez un pseudo et cliquez ci-dessous</p>
        <!-- formulaire d'authentification -->
            <form action="#" method="POST">
                <input type="text" id="pseudo" class ="champ_form" name="pseudo" required placeholder="Écrivez votre pseudo ici" > <br>
                <input type="submit"class="bg-primary p-2 col-xl-3 col-sm-6 rounded text-uppercase" name="validerPseudo" value=Entrer></a>
            </form>
            <?php validationPseudo(); ?>
        </div>
</body>
</html>