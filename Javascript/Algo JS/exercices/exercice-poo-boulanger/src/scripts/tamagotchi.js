var monTama;

var boutonManger = document.querySelector("#boutonManger");
boutonManger.style.display = "none";

class Tamagotchi {
    _nom;
    _genre;
    _jaugeDeVie;

    constructor(nom, genre) {
        this._nom = nom;
        this._genre = genre;
        this._jaugeDeVie = 100;

        console.log("Creation du Tamagotchi: ", nom, genre);
        this.init();
    }

    init() {
        var img = document.createElement("img");
        if (this._genre == "femme") {
            img.src = "img/tamaGirl.jpg";
        }
        else if (this._genre == "homme") {
            img.src = "img/tamaBoy.jpg";
        }
        var container = document.querySelector("container");
        container.appendChild(img);

        setInterval("perdreDeLaVie()", 1000);
    }

    set jaugeDeVie(value) {
        this._jaugeDeVie = value;
    }

    get jaugeDeVie() {
        return this._jaugeDeVie;
    }

    manger() {
        this.jaugeDeVie++;
    }
}

let boutonEnvoyer = document.querySelector("#boutonEnvoyer");
// // let boutonEnvoyer2 = document.getElementById("boutonEnvoyer");
boutonEnvoyer.addEventListener("click", creationDuTamagotchi);

function creationDuTamagotchi() {
    evt.preventDefault();

    let inputNom = document.querySelector("#nom");
    let inputGenre = document.querySelector("#genre");

    let nom = inputNom.value;
    let genre = inputGenre.value;

    // si la valeur est égale à rien "" alors c'est qu'il n'y a pas de nom
    // == égal
    // != négation
    // && et
    // || ou
    if (nom == "") {
        console.error("Attention : pas de nom");
        return;
    }

    if (genre != "femme" && genre != "homme") {
        console.error("Attention : pas de genre");
        return;
    }

    // if(nom == "Paula" || nom == "Paulo") {
    //     console.error("Hey t'es mon ou ma meilleur.e ami.e !");
    // }

    monTama = new Tamagotchi(nom, genre);

    let formulaire = document.querySelector("form");
    formulaire.style.display = "none";

    boutonManger.style.display = "block";
    // on n'oublie pas de passer la référence de this à la fonction sinon il est perdu
    boutonManger.addEventListener("click", () => monTama.manger(this));
}

function perdreDeLaVie() {
    monTama.jaugeDeVie -= 1;
    console.log("Jauge de vie", monTama.jaugeDeVie)

    if (monTama.jaugeDeVie == 0) {
        console.error("GAME OVER");
    }
}

// function myFunction() {
//     console.log("onsubmit");
//     event.preventDefault();
// }