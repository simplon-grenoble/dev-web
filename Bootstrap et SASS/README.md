# Bootstrap et SASS

## Ressources SASS

- [SASS c'est quoi ?](https://www.wenovio.com/2022/05/10/sass-c-est-quoi/)
- [L'antisèche SASS à télécharger](https://itexpert.fr/blog/antiseche-sass/)
- [Utiliser @use ou @import ?](https://css-tricks.com/introducing-sass-modules/)
- [Antisèche Bootstrap 5 et Mixins SASS](https://gist.github.com/anschaef/09c5426ce1619b381b9c4297a6fc0914)
- [Masterclass SASS](https://github.com/aymanebenhima/sass_masterclass)