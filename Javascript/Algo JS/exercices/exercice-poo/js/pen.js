/**
 * L'objet pen que l'on a fait dans exercice-object.js est techniquement
 * correct mais non réutilisable à moins de dupliquer une bonne
 * partie de son code.
 * Si l'on a un objet dont on voudra avoir plusieurs occurrences (instances)
 * on pourra alors créer une classe à la place.
 * Une classe est un genre de moule/usine à partir de laquelle on
 * pourra généré des objets utilisables (instances) qui auront la
 * classe comme modèle.
 */

    /**
     * Pour spécifier les propriétés d'une classe en javascript
     * on devra le faire à l'intérieur d'un constructeur. C'est la
     * méthode qui sera appelée lorsque l'on créera une instance de
     * la classe en question.
     * Ici, note constructeur attend 3 arguments dont un avec une
     * valeur par défaut. On assigne ces 3 arguments aux propriétés
     * de la classe Pen via le mot clef this
     */

    /**
     * La méthode write va créer un élément p de la couleur et de
     * la taille des propriétés du stylo utilisé et mettre du texte
     * à l'intérieur.
     * @param {string} text Le texte à écrire dans le html
     */
