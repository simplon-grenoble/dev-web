import { Bread } from "./Bread";

export class Baker{

    _nom;
    _quantiteDeFarine;

    constructor(nom, quantiteDeFarine){
        this._nom = nom;
        this._quantiteDeFarine = quantiteDeFarine;
    }

    get nom(){
        return this._nom;
    }

    set nom(nom){
        this._nom = nom;
    }

    get quantiteDeFarine(){
        return this._quantiteDeFarine;
    }

    set quantiteDeFarine(quantiteDeFarine){
        this._quantiteDeFarine = quantiteDeFarine;
    }

    // ALT + Z pour retour à la ligne
    cuireDesPains(quantiteDePain){
        // Faire que la méthode bakeBreads crée un tableau vide 
        let fournee = [];
        // puis fasse une boucle qui fera quantiteDePain tours 
        for(let i = 0; i < quantiteDePain; i++){
            // le boulanger fait du pain uniquement s'il lui reste de la farine (quantiteDeFarine supérieur à 0).
            if(this.quantiteDeFarine > 0){
                // et qui à chaque tour de boucle créera une instance de Bread, 
                let pain = new Bread(250, "baguette");
                // déclenchera sa méthode cook et le mettra dans le tableau. 
                pain.cook(300);
                // on ajoute le pain au tableau de la fournée
                fournee.push(pain);
                // chaque fois qu'on fait un pain, on baisse la valeur de la propriété quantiteDeFarine de 1 
                // this.quantiteDeFarine -= 1;
                this.quantiteDeFarine--;
            }            
        }   

        // A la fin de la boucle, la méthode return le tableau de Bread.
        return fournee;
    }

}