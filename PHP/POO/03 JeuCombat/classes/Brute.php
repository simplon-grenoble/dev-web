<?php

class Brute extends Personnage {

  // Attributs
  //
  // Constantes de classe :
  const FORCE = 6;
  const RESISTANCE = 4;



  //Constructeur
  //
  public function __construct(Array $donnees)
  {
    parent::__construct($donnees);
  }

  protected function subirAttaque(int $degats,float $gainResistance,object $attaquant,int $nbattaque=0){

    If($attaquant->getType() == "Mage"){
      $gainResistance = $this->getResistance()*0.3;
      $this->setResistance($gainResistance);
      $this->setVie($degats);
    }else{
      parent::subirAttaque($degats,$gainResistance, $this);
    }
    if ($attaquant->getType() !== "Archer" && $nbattaque < 1) {
      $nbattaque ++;
      self::attaque($attaquant,$nbattaque);
    }
  }

    public function CrierMonType(){
    echo "Je suis un ".get_class()." et je suis le plus fort !!";
  }
}
