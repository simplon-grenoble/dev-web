import { useState } from "react";
import TaskForm from "./TaskForm";
import TaskList from "./TaskList";

function ParentTaskManager() {
  const [tasks, setTasks] = useState([
    { id: 1, name: "Faire la lessive", completed: false },
    { id: 2, name: "Acheter des légumes", completed: true },
    { id: 3, name: "Appeler le dentiste", completed: false },
  ]);

  function handleTasksChange(updatedTasks) {
    setTasks(updatedTasks);
  }

  return (
    <div>
      <div>Task Manager</div>
      {/* tasksProp (qu'on appelerait normalement task) c'est le nom de la prop qu'on passe à TaskList, c'est à dire qu'on va récupérer là bas avec props.tasksProp. 
      
      tasks c'est le tableau qu'on a stocké dans l'état interne (le state) de notre TaskManager. La fonction setTasks est utilisée pour mettre à jour l'état. */}
      <TaskList tasksProp={tasks} onTasksChange={handleTasksChange} />
      <TaskForm tasksProp={tasks} onTasksChange={handleTasksChange} />
    </div>
  );
}

export default ParentTaskManager;
