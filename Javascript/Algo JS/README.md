# Ressources Algo JS
Exercices pour s'entraîner aux algos avec Javascript.

## [Exercices JS] https://github.com/tyc45/exercices-js
- [Let, var et const : 3 exercices](https://fr.javascript.info/variables#tasks)
- [Les types de données : 1 exercice](https://fr.javascript.info/types#tasks)
- [Alert, prompt et confirm : 1 exercice](https://fr.javascript.info/alert-prompt-confirm#tasks)
- [Opérateurs de base, mathématiques : 4 exercices](https://fr.javascript.info/operators#tasks)
- [Comparaisons : 1 exercice](https://fr.javascript.info/comparison#tasks)
- [Branche conditionnelle : if, '?' : 5 exercices](https://fr.javascript.info/ifelse#tasks)
- [Opérateurs logiques : 9 exercices](https://fr.javascript.info/logical-operators#tasks)
- [Boucles : while et for : 7 exercices](https://fr.javascript.info/while-for#tasks)
- [La déclaration "switch" : 2 exercices](https://fr.javascript.info/switch#tasks)