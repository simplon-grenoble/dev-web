class User {

    _id;
    _nom;
    _motDePasse;
    _email;
    _email;
    _avatar;

    constructor(nom, motDePasse, email, dateDeNaissance, avatar) {
        // this._id = new Date().valueOf();
        this._nom = nom;
        this._motDePasse = motDePasse;
        this._email = email;
        this._dateDeNaissance = dateDeNaissance;
        this._avatar = avatar;
    }

    get nom() {
        return this._nom;
    }

    get motDePasse() {
        return this._motDePasse;
    }

    get email() {
        return this._email;
    }

    get dateDeNaissance() {
        return this._dateDeNaissance;
    }

    get avatar() {
        return this._avatar;
    }

    /**
     * La méthode toHTML va créer un élément HTML représentant
     * le user actuel et va le return pour qu'il soit utilisé à
     * l'extérieur
     */
    toHTML() {
        // https://developer.mozilla.org/fr/docs/Web/API/Document/createElement
        let para = document.createElement("p");
        // para.innerText = this._nom + " " + this._motDePasse + " " + this._email;
        para.innerText = `${this._nom} ${this._motDePasse} ${this._email}`;
        return para;
    }

}