//import React, { useState } from "react";
import React, { useReducer } from "react";
import styled from "styled-components";

const ColoredBackground = styled.div`
  background-color: red;
`;

// Grâce au hook useState, vous devez définir un état local qui va vous permettre de savoir si une boite de dialogue (fenêtre modale) est ouverte. Vous aurez pour cela une fonction qui gérera l'ouverture et la fermeture de la boîte.

// Au niveau de l'affichage, un bouton "Open" et un bouton "Close" (qui appraraît dans la fenêtre), ou alors si vous préférez un seul bouton dont l'affichage change. :)

// Dans la fenêtre s'affiche un champ de texte dont on va récupérer le contenu ensuite.

// function Modal() {
//   // LA VARIABLE : isOpen est un boolean, si true la fenêtre est ouverte, si false elle est fermée
//   // LA METHODE / FONCTION : setIsOpen permet de modifier la valeur de isOpen, qui est à false au départ
//   const [isOpen, setIsOpen] = useState(false);
//   const [text, setText] = useState("");

//   function handleClick() {
//     setIsOpen(!isOpen);
//   }

//   function handleChange(event) {
//     setText(event.target.value.toUpperCase());
//   }

//   return (
//     <div>
//       <button onClick={handleClick}>
//         {isOpen ? "Fermer la fenêtre" : "Ouvrir la fenêtre"}
//       </button>

//       {isOpen && (
//         <div id="modal">
//           <input type="text" value={text} onChange={handleChange} />
//         </div>
//       )}
//     </div>
//   );
// }

function Modal() {
  // L'état initial du composant, qui est un objet avec deux propriétés :
  // isOpen initialisé à false et text initialisé à une chaîne vide.
  const initialState = {
    isOpen: false,
    text: "",
  };

  /* useReducer est un hook, il permet de gérer l'état d'un composant à l'aide d'une fonction de réduction (en anglais, "reducer"). Il prend deux arguments : la fonction de réduction reducer qui définit comment l'état doit être mis à jour en réponse à une action, et l'état initial initialState qui définit l'état initial du composant. Il renvoie un tableau de deux éléments :

  - Le premier élément, state, est l'état actuel du composant, qui est un objet avec deux propriétés (isOpen et text)
  - Le deuxième élément, dispatch, est une fonction qui permet de déclencher une action pour mettre à jour l'état (elle est utilisée dans les fonctions handleSomething).

  Lorsqu'une action est déclenchée à l'aide de dispatch, la fonction reducer est appelée avec l'état actuel et l'action. La fonction reducer renvoie un nouveau state qui remplace l'ancien. Ce nouveau state est alors stocké dans state, et le composant est mis à jour pour refléter le nouveau state.
  */
  const [state, dispatch] = useReducer(reducer, initialState);

  /*  ...state est un opérateur de décomposition d'objet en JavaScript. Dans le contexte de la gestion de l'état dans les applications React, il est utilisé pour créer une copie immuable de l'objet d'état existant avant de modifier une ou plusieurs de ses propriétés.

  En utilisant l'opérateur ... suivi du nom de l'objet (dans ce cas, state), on crée une nouvelle instance de l'objet avec les mêmes propriétés et valeurs que l'objet original. Cependant, la nouvelle instance est une copie distincte et indépendante de l'objet original, ce qui permet de garantir l'immutabilité de l'état.

  Dans l'expression return {...state, isOpen : !state.isOpen}, cela signifie que nous créons une nouvelle copie de l'objet d'état state avec toutes ses propriétés existantes, puis en ajoutant/modifiant la propriété isOpen avec sa valeur inversée. Le résultat est un nouvel objet avec toutes les propriétés de l'objet d'état original, mais avec une valeur différente pour la propriété isOpen.
*/
  function reducer(state, action) {
    switch (action.type) {
      case "OPEN_CLOSE_MODAL":
        return { ...state, isOpen: !state.isOpen };
      case "SET_TEXT":
        return { ...state, text: action.value };
      default:
        return state;
    }
  }

  function handleClick() {
    dispatch({ type: "OPEN_CLOSE_MODAL" });
  }

  function handleChange(event) {
    dispatch({ type: "SET_TEXT", value: event.target.value.toUpperCase() });
  }

  return (
    <ColoredBackground>
      <button onClick={handleClick}>
        {state.isOpen ? "Fermer la fenêtre" : "Ouvrir la fenêtre"}
      </button>

      {state.isOpen && (
        <div id="modal">
          <input type="text" value={state.text} onChange={handleChange} />
        </div>
      )}
    </ColoredBackground>
  );
}

export default Modal;
