/*
 *On crée ici un objet qui représente un stylo, en lui spécifiant
 une couleur, une taille et un type de stylo ainsi qu'une méthode
 pour écrire 
 */

        //On peut accéder aux propriétés de l'objet dans lequel
        //on se situe actuellement en utilisant le mot clef this
        //Ici, on l'utilise pour modifier la couleur et la taille
        //du paragraphe à écrire en se basant sur les valeurs de l'objet
