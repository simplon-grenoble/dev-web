/**
 * Webpack va permettre de prendre les fichiers javascript (entre 
 * autre) à partir d'un point d'entrée, et de tous les mettre dans
 * un seul fichier bundle qu'on chargera dans le html, nous évitant
 * le fait de charger tous les scripts dans le bon ordre.
 * En plus de ça, il permettra de faire du javascript modulaire 
 * dans nos fichier js. (voir index.js)
 */
module.exports = {
    //Le mode indique si on est en dev ou en prod, ça influera sur
    //la taille du fichier obtenu et sur la vitesse de compilation entre autre
    mode: 'development',
    //On indique le point d'entrée de notre appli, le script duquel
    //tout partira.
    entry: './src/js/index.js',
    output: {
        //on lui indique dans l'output le nom du bundle qui sera
        //généré dans le dossier dist
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader"
        }]
    },
    //On lui précise cette ligne afin que les navigateurs fassent
    //correctement le lien entre les lignes du bundle et les
    //lignes de nos sources
    devtool: 'inline-source-map'
};