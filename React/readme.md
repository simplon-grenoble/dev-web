Voici plein de liens pour des questions avancées en React :)

# Tests unitaires en React
- https://openclassrooms.com/fr/courses/7150606-creez-une-application-react-complete/7256627-decouvrez-la-base-des-tests-dans-react-avec-jest
- https://www.freecodecamp.org/news/write-unit-tests-using-react-testing-library/
- https://www.freecodecamp.org/news/how-to-test-react-applications/ 

# SEO : le référencement c'est important !

## SEO général
- https://ahrefs.com/blog/seo-issues/
- https://ahrefs.com/blog/mobile-seo/
https://slashr.fr/maillage-interne/

## SEO pour React
- https://ahrefs.com/blog/react-seo/
- https://clockwise.software/blog/seo-for-react-apps/
- https://yalantis.com/blog/search-engine-optimization-for-react-apps/
- https://www.novatis-paris.fr/react-seo-et-ses-meilleures-pratiques-doptimisation/
- https://slashr.fr/react-js-seo/

## React Helmet
- https://github.com/nfl/react-helmet
- https://amagiacademy.com/blog/posts/2020-09-05/structured-data-with-react-helmet
- https://www.fullstacklabs.co/blog/improving-seo-in-react-apps-with-react-helmet
- https://www.geeksforgeeks.org/react-helmet-seo-for-reactjs-apps/
- https://www.freecodecamp.org/news/react-helmet-examples/
- https://www.ohmycrawl.com/react/helmet/

## React Router Sitemap
- https://www.npmjs.com/package/react-router-sitemap
- https://www.amitsn.com/blog/how-to-generate-a-sitemap-for-your-react-website-with-dynamic-content
- https://william-lindsay.medium.com/serving-a-sitemap-in-react-for-seo-purposes-add972bb8dfc
- https://codesandbox.io/examples/package/react-router-sitemap

## Next.js pour le SEO
- https://snipcart.com/blog/react-nextjs-single-page-application-seo
- https://medium.com/free-code-camp/applied-react-seo-on-a-next-js-app-live-demo-cc7e3c6522b3
- https://levelup.gitconnected.com/make-your-next-js-app-seo-friendly-with-dynamic-routes-71e0fd1adb92
- https://themeptation.medium.com/next-js-the-ultimate-guide-to-achieving-100-seo-and-performance-572cd701cda1
- https://medium.com/@akashjha9041/the-biggest-next-js-boilerplates-of-2023-445f8dc65f0f
- https://blog.logrocket.com/create-react-app-vs-next-js-performance-differences/
- https://blog.eleven-labs.com/fr/comment-construire-site-web-avec-nextjs/
- https://nextjs.org/learn/seo/rendering-and-ranking
- https://dhanrajsp.me/blog/the-troubles-with-nextjs
- https://kinsta.com/blog/nextjs-vs-react/
- https://www.ohmycrawl.com/nextjs/seo/

# Création d'une API Rest en Node.js / Express
- https://openclassrooms.com/fr/courses/6573181-adoptez-les-api-rest-pour-vos-projets-web
- https://welovedevs.com/fr/articles/node-js-api/
- https://www.kilukru.dev/construire-une-api-rest-a-laide-de-node-js-express-js-et-mongodb-et-la-tester-sur-postman/
- https://www.alsacreations.com/tuto/lire/1857-Creation-dune-API-REST-avec-Express-et-TypeScript.html 