<?php
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Hash de mot de passe</title>
</head>
<body>
  <form>
    <label for="">Mot de passe à hasher : </label><input type="text" id="mdp">
    <input type="button" value="allons-y !" onclick="hashmdp()">
  </form>

  <div id="progress"></div>
  <div id="resultat"></div>

  <script>
    function hashmdp(){
      var mdp = document.getElementById('mdp').value;
      var progress = document.getElementById('progress');
      var resultat = document.getElementById('resultat');

      // J'instancie l'objet XMLHttpRequest, pour lancer une requête Ajax
      var req = new XMLHttpRequest();

      // J'ouvre la connexion, en lui donnant :
      // 1. la méthode d'envoi
      // 2. le chemin de traitement
      // 3. si la requête est asynchrone
      req.open("POST","traitement.php",true);

      // Je donne ensuite le header permettant de connaitre comment le format sera encodé
      req.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

      // var data = JSON.stringify({'mdp': mdp, 'hash':'md5'})
      var data = "mdp=" + mdp + "&hash=md5";

      // j'envoie enfin les infos.
      req.send(data);

      // J'écoute ensuite l'état actuel de la requête :
      // Voir les états possibles : https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest/readyState
      req.onreadystatechange = function(){
      for (var i = 1; i < 5; i++) {

          if (req.readyState === 1) {
            progress.innerHTML = '<progress value="0" max="100"></progress><span id="SpanProgressBar"> 0%</span>'; }
          if (req.readyState === 2) {
            progress.innerHTML = '<progress value="25" max="100"></progress><span id="SpanProgressBar"> 25%</span>'; }
          if (req.readyState === 3) {
            progress.innerHTML = '<progress value="50" max="100"></progress><span id="SpanProgressBar"> 50%</span>'; }
          if (req.readyState === 4) {
            progress.innerHTML = '<progress value="75" max="100"></progress><span id="SpanProgressBar"> 75%</span>'; }
        }

        // Une fois que la réponse a été entièrement reçue, on vérifie qu'on la bien recue en 200 (et pas 404 ou autre);
        //
        //
        if ((req.readyState === 4) && (req.status === 200)) {
          progress.innerHTML = '<progress value="100" max="100"></progress><span id="SpanProgressBar"> 100%</span>';
          //
          // Enfin, j'affiche grâce à responseText, la réponse renvoyée.
          resultat.innerHTML += req.responseText;
        }
        if(req.status === 404){
          resultat.innerHTML = "le fichier de traitement n'est pas trouvé";
        }
    }
  }
  </script>
</body>
</html>
