<?php

final class Mage extends Personnage {

  // Attributs
  //
  // Constantes de classe :
  const FORCE = 2;
  const RESISTANCE = 2;
  const GAIN_VIE = 2;

  public function __construct(Array $donnees)
  {
    parent::__construct($donnees);

  }

  public function attaque($victime, int $nbattaque=0){
    parent::attaque($victime);
    $this->setVie(Mage::GAIN_VIE);
  }

  protected function subirAttaque(int $degats,float $gainResistance,object $attaquant, int $nbattaque=0){
    If($attaquant->getType() == "Sabreur"){
      $gainResistance = 0;
      $this->setResistance($gainResistance);
      $this->setVie($degats);
    }else{
      parent::subirAttaque($degats,$gainResistance,$this);
    }
  }

  public function CrierMonType(){
    echo "Je suis un ".get_class()." et je suis le meilleur !!";
  }

  final public function LancerSort(){

  }

}
