<?php
/*
Plugin Name: Connect Log
Plugin URI:
Author URI: https://unsiteavous.fr/realisations/extensions/connect-log
Description: Connect Log permet de garder une trace de la dernière connexion.
Author: Théophile
Version: 0.0
Requires at least: 5.6
Tested up to: 6.1.1
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Author URI: https://unsiteavous.fr
Text Domain: connect-log
*/

// Enregistrer la date dans la BDD
function CL_enregistrement($user_login, WP_User $user){
  date_default_timezone_set('Europe/Paris');
  $id = $user->ID;
  $dateAEnregistrer = dateToFrench('now', 'd F Y - H:i:s');
  if (get_option('connectlog_'.$id)) {
    update_option('connectlog_'.$id, $dateAEnregistrer);
  } else {
    add_option('connectlog_'.$id, $dateAEnregistrer);
  }
  return $dateAEnregistrer;
}

add_action('wp_login','CL_enregistrement',10,2);

// Récupérer la date de la connexion soit dans la base de données si elle existe, soit prendre la date actuelle
// et Afficher la date dans le tableau de bord d'admin

function CL_afficherLog(){
  $id = get_current_user_id();
  if (get_option('connectlog_'.$id)) {
    $dateDerniereConnexion = get_option('connectlog_'.$id);
  } else {
    $dateDerniereConnexion = CL_enregistrement();
  }
  printf(
      /* Traducteur : %s est la date de la dernière connexion.*/
      __('<p class="connectlog">Dernière connexion : %s</p>','connect-log'),
      $dateDerniereConnexion
    );
}

add_action('admin_notices', 'CL_afficherLog');

function CL_css() {
  echo "
  <style type='text/css'>
  .connectlog {
    float: right;
    padding: 5px 10px;
    margin: 0;
    font-size: 12px;
    line-height: 1.6666;
  }
  .block-editor-page .connectlog {
    display: none;
  }
  @media screen and (max-width: 782px) {
    .connectlog {
      float: none;
      padding-left: 0;
      padding-right: 0;
    }
  }
  </style>
  ";
}

add_action( 'admin_head', 'CL_css' );



function dateToFrench($date, $format)
{
    $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
    $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
    $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
    return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
}
