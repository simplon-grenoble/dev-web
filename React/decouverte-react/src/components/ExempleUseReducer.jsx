import { useReducer } from "react";

/* Une Promise est un objet JavaScript qui représente la valeur future ou l'achèvement ou l'échec d'une opération asynchrone. Elle est utilisée pour gérer les opérations asynchrones en JavaScript. Les Promises sont des objets qui peuvent être en trois états :

- pending : la promesse est en cours de traitement et le résultat n'est pas encore disponible.
- fulfilled : la promesse a réussi et le résultat est disponible.
- rejected : la promesse a échoué et une raison de l'échec est disponible.

La méthode .then() est utilisée pour accéder à la valeur de la promesse résolue. Elle prend une ou deux fonctions en argument, l'une qui est appelée si la promesse est résolue et l'autre qui est appelée si la promesse est rejetée 

La méthode .catch() est utilisée pour gérer les erreurs lorsqu'une promesse est rejetée.*/
function generateName() {
  // Retourne une promesse résolue avec un nom aléatoire après une seconde de délai.
  return new Promise((resolve) => {
    setTimeout(() => {
      const names = [
        "Kayila",
        "Lucas",
        "Mailys",
        "Alexandre",
        "Christian",
        "Gaël",
        "Mickael",
        "Boubacar",
        "Olivier",
        "Aleksandra",
        "Linda",
        "Sélim",
      ];
      const randomIndex = Math.floor(Math.random() * names.length);
      resolve(names[randomIndex]);
    }, 1000);
  });
}

// Retourne un composant qui affiche "Loading..." pour indiquer que quelque chose est en train de se charger.
function Loader() {
  return <div>Loading...</div>;
}

// La fonction reducer est un gestionnaire d'action qui prend en entrée un état et une action, et retourne un nouvel état en fonction du type de l'action. Ici, l'action peut être de deux types : "startGenerateName" et "endGenerateName".
function reducer(state, action) {
  switch (action.type) {
    // Si l'action est de type "startGenerateName", la propriété loading de l'état est mise à true.
    case "startGenerateName":
      return { ...state, loading: true };
    // Si l'action est de type "endGenerateName", la propriété name de l'état est mise à la valeur newName fournie dans l'action, et la propriété loading est mise à false.
    case "endGenerateName":
      return { ...state, name: action.newName, loading: false };
    // Si l'action n'est d'aucun des deux types, une erreur est levée.
    default:
      throw new Error();
  }
}

// Ce code utilise le hook useReducer de React pour gérer l'état d'un composant nommé ExemplesState. L'état initial de ce composant est défini comme un objet avec trois propriétés : name initialisée à "MacGuffin", click initialisée à 0, et loading initialisée à false. Le tableau destructuré { loading, name } contient les valeurs actuelles des propriétés loading et name de l'état.
const ExempleUseReducer = () => {
  const [{ loading, name }, dispatch] = useReducer(reducer, {
    name: "Kayila",
    click: 0,
    loading: false,
  });

  // Lorsque l'utilisateur clique sur le div retourné par le composant, la fonction handleClick est appelée. dispatch est une fonction qui permet d'envoyer des actions au gestionnaire d'action reducer.
  const handleClick = () => {
    // Envoie d'abord une action de type "startGenerateName" au gestionnaire d'action reducer en appelant la fonction dispatch.
    dispatch({ type: "startGenerateName" });

    // Lorsque la promesse est résolue, la fonction then est appelée avec le nom aléatoire comme argument, et cette fonction envoie une action de type "endGenerateName" au gestionnaire d'action reducer, avec le nom aléatoire comme valeur de la propriété newName dans l'action.
    generateName().then((newName) => {
      dispatch({ type: "endGenerateName", newName });
    });
  };

  // Le composant est alors rendu avec le nom aléatoire affiché, et la propriété loading est mise à false pour indiquer que le chargement est terminé. Si la propriété loading est true, le composant Loader est rendu à la place pour indiquer que le chargement est en cours.
  return loading ? <Loader /> : <div onClick={handleClick}>{name}</div>;
};

export default ExempleUseReducer;
