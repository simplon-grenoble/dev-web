<?php
session_start();


if (isset($_POST['input'])) {
    if ($_POST['input'] === 'ROCKY') {
        $_SESSION['score']++;
        header('Location: cinema8.php');
        exit;
    } else if ($_POST['input'] === 'BATMAN') {
        header('Location: cinema8.php');
        exit;
    } else if ($_POST['input'] === 'TERMINATOR') {
        header('Location: cinema8.php');
        exit;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<body id="corn" class="p4">
    <img class="pop" src="src/pop.png">
    <audio class="song" autoplay controls>
    <source src="src/Rocky.mp3" type="audio/mpeg">
    </audio>
    <p class="txt2">De quel film est tirée cette musique ?</p>
    <form method="post">
        <input class="cinema1" type="submit" name="input" value="ROCKY">
        <input class="cinema1" type="submit" name="input" value="BATMAN">
        <input class="cinema1" type="submit" name="input" value="TERMINATOR">
    </form>
</body>
</html>