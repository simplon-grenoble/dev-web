<?php
// j'utilise session_start car j'ai des variables qu'il faut récupérer sur toutes les pages
session_start();

// j'ai 3 variables, $score pour la première série de 10 questions, $scores (scoreSupérieur) pour le niveau supérieur et $scorei (scoreInférieur)
$score=0;
$_SESSION['score'] = $score;
$scores=0;
$_SESSION['scores'] = $scores;
$scorei=0;
$_SESSION['scorei'] = $scorei;

// si l'on clique sur start, on est redirigé vers la question 1 avec la fonction header
if (isset($_POST['input'])) {
    if ($_POST['input'] == "START") {
        header('Location: cinema1.php');
        exit;
    }
}

  ?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quiz.css" type="text/css">
    <script src="quiz.js"></script>
    <title>QUIZ</title>
</head>
<!-- j'ai mis le même id sur le body de chaque page pour récupérer le JS de mon image pop -->
<body id="corn" class="p1">
        <img class="pop" src="src/pop.png">
        <div class="pp1">
            <h1>Bienvenue à vous sur cette page</h1>
            <p>Je suis Pop le Popcorn et je ne vous lâcherai pas !<br> Je vous propose de faire un quiz sur le cinéma qui se déroulera de la manière suivante:<br> une première série de 10 questions assez simple et en fonction de vos réponses, s'en suivra une autre série de 5 questions plus adaptées à votre niveau.<br> Je vous souhaite bonne chance ! </p>
        </div>
        <form method="post">
            <input class="cinemaccueil" type="submit" name="input" value="START">
        </form>
</body>
</html>

