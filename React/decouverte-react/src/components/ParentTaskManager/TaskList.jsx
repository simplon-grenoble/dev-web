// TaskList : affiche une liste de tâches et permet à l'utilisateur de marquer une tâche comme terminée.
function TaskList(props) {
  /* Cette fonction, handleChange, sera appelée lorsque l'utilisateur coche une case à cocher (checkbox) pour marquer une tâche comme terminée. Elle prend l'ID de la tâche en question comme argument et met à jour l'état des tâches en définissant la propriété "completed" à true pour la tâche correspondante. Elle appelle ensuite la fonction onTasksChange, fournie via les props, pour mettre à jour la liste de tâches dans l'état de la TaskManager parente. 
  
  La principale différence entre la syntaxe const handleChange = (id) => {...} et la syntaxe function handleChange(id) {...} est la façon dont la fonction est définie et la façon dont le mot-clé this est géré. En utilisant la syntaxe de fonction fléchée, le mot-clé this est hérité de la portée dans laquelle la fonction est définie. En revanche, avec la syntaxe de déclaration de fonction, le mot-clé this est lié à la portée de la fonction elle-même. */
  const handleChange = (id) => {
    let updatedTasks = props.tasksProp.map((task) => {
      if (id === task.id) {
        return { ...task, completed: true };
      } else {
        return task;
      }
    });
    props.onTasksChange(updatedTasks);
  };

  /* Dans la partie return de la fonction, nous avons un élément de liste non ordonnée qui contient une liste d'éléments de liste générés à partir de la liste de tâches fournies via les props. Chaque élément de liste contient une case à cocher (checkbox) et le titre de la tâche. Lorsque l'utilisateur coche une case à cocher, la fonction handleTaskCompletion est appelée pour mettre à jour l'état de la TaskManager parente.*/
  return (
    <div>
      <ul>
        {props.tasksProp.map((task) => (
          <li key={task.id}>
            <input
              type="checkbox"
              checked={task.completed}
              onChange={() => handleChange(task.id)}
            />
            {task.name}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TaskList;
