// Installer la dépendance PropTypes : npm install prop-types
// https://www.npmjs.com/package/prop-types 
// https://fr.reactjs.org/docs/typechecking-with-proptypes.html#gatsby-focus-wrapper

import React from "react";
import PropTypes from "prop-types";

function ExemplePropType(props) {
  return (
    <div>
      <h1>Bonjour, {props.name}!</h1>
      <p>Tu as {props.age} ans.</p>
    </div>
  );
}

ExemplePropType.propTypes = {
  name: PropTypes.string.isRequired,
  age: PropTypes.number,
};

export default ExemplePropType;
